#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created [2023-08-10 Thu 15:32]

@author: tristan
"""

from time import time
from pandas import Timestamp


def get_org_timestamp():
    now = time()
    stamp = Timestamp(now + 7200, unit="s")
    # day = stamp.day_name()[:3]

    return "[" + stamp.strftime("%Y-%m-%d %a %X")[:-3] + "]"


def head_log():
    """Start new entry like '* [2024-06-06 Thu 20:23]'."""
    headline = "* " + get_org_timestamp()
    log(headline)


def log_chronological(s, caller):
    log_under_heading(caller + ": " + s, "Chronological")


def log_under_heading(message, heading):
    heading = "** " + heading
    with open("log.org", "r") as log_file:
        contents = log_file.readlines()

    for i in range(len(contents)):
        line = contents[-(i + 1)]
        if "* " == line[:2]:
            starting_point = len(contents) - i
            break

    contents_part = contents[starting_point:]

    if heading + "\n" in contents_part:
        heading_index = contents.index(heading + "\n", starting_point)

        next_heading_index = heading_index
        for line in contents[heading_index + 1 :]:
            next_heading_index += 1
            if "* " in line:
                break

        contents.insert(next_heading_index, message + "\n")
        with open("log.org", "w") as log_file:
            log_file.writelines(contents)
    else:
        log(heading)
        log(message)


def log(message: str, heading: str = None):
    """
    Log a string (message) into an organized logbook.
    """
    if heading is None:
        with open("log.org", "a") as log_file:
            log_file.write(message + "\n")
        return 0

    else:
        log_chronological(message, heading)
        log_under_heading(message, heading)
        return 0


def print_log():
    """
    Print the log to command line from the last timestamp on.
    """
    with open("log.org", "r") as log_file:
        contents = log_file.readlines()

    for i in range(len(contents)):
        line = contents[-(i + 1)]
        if "* " == line[:2]:
            starting_point = len(contents) - i
            break

    contents_part = contents[starting_point:]

    print("".join(contents_part))


if __name__ == "__main__":
    head_log()
    log("state1", "tui")
    log("state2", "gui")
    log("state3", "tui")
    log("state4", "tui")
