#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on [2023-09-08 Fri 21:33]

@author: tristan
"""

elements = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na',
            'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti',
            'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge',
            'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo',
            'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te',
            'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm',
            'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf',
            'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb',
            'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U',
            'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No',
            'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn',
            'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og']

properties = ["Value", "Min", "Max", "Radius", "Fixed", "Precision"]

sto = {"Name": "STO",
       "Formula": "SrTiO3",
       "Custom": False,
       "x": 1,
       "y": 1,
       "z": 1,
       "Atoms": {
           "Sr": [
               0.0,
               0.0,
               0.0
           ],
           "Ti": [
               0.5,
               0.5,
               0.5
           ],
           "O1": [
               0.0,
               0.5,
               0.5
           ],
           "O2": [
               0.5,
               0.0,
               0.5
           ],
           "O3": [
               0.5,
               0.5,
               0.0
           ]
       },
       "Code": ""
       }
