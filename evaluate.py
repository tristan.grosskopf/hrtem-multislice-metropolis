#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 09:00:10 2021

@author: tristan
"""

from os import listdir, mkdir
from os.path import isdir
from dmanager import DataManager
import matplotlib.pyplot as plt
import numpy as np
import json


class Evaluate:
    def __init__(self):
        self.std_src = "data/results/"
        self.std_trg = "data/evaluation/"

    def evaluate_all(self):
        for f in listdir(self.std_src):
            if isdir(self.std_trg + f):
                continue
            else:
                self.evaluate(self.std_src + f)

    def evaluate_latest(self):
        src = sorted(listdir(self.std_src))[-1]

        self.evaluate(self.std_src + src)

    def evaluate(self, src):
        if isdir(src):
            dir_name = src.split("/")[-1]
            target = self.std_trg + dir_name + "/"

            if not isdir(target):
                mkdir(target)

            save_file = self.files_ending_with("json", src)
            if save_file is not None:
                save_file = save_file[0]

                copy = open(target + save_file, "w")
                with open(src + "/" + save_file, "r") as f:
                    for line in f:
                        copy.write(line)
                copy.close()

            if src.split("_")[-1] == "G":
                self.evaluate_group(src + "/", target, save_file)

                # best_target = self.std_trg + dir_name + "_B/"
                # if not isdir(best_target):
                #     mkdir(best_target)
                # self.evaluate_best(src + "/", best_target, save_file)
            else:
                self.evaluate_single(src + "/", target, save_file)

        else:
            return 0

    def evaluate_single(self, src, trg, svf):
        csv = self.files_ending_with("csv", src)
        if csv is not None:
            csv = csv[0]
        else:
            return 0

        text = []
        head = ""
        with open(src + csv, "r") as f:
            head = f.readline()
            for line in f:
                text.append(line)

        head = head.split(",")
        head[-1] = head[-1][:-1]

        results = np.loadtxt(text[:-1], delimiter=",")
        data = {p: results[:, i] for i, p in enumerate(head)}

        for parameter in data:
            if parameter not in ["Time", "X Offset", "Y Offset"]:
                fig = plt.figure(dpi=200)
                if parameter in ["Residuum", "Determinant"]:
                    plt.semilogy()
                plt.plot(np.arange(len(data[parameter])), data[parameter])
                if svf is not None:
                    plt.title(svf[:-5])
                plt.xlabel("Iterationen")
                plt.ylabel(parameter)
                plt.tight_layout()
                fig.savefig(trg + parameter + ".png", dpi=fig.dpi)
                plt.close()

    def evaluate_group(self, src, trg, svf):
        data = {}
        head = []
        dirs = [d + "/" for d in sorted(listdir(src)) if isdir(src + d)][:-1]

        for drc in dirs:
            results_file = self.files_ending_with("csv", src + drc)
            if results_file is not None:
                results_file = results_file[0]
            else:
                continue

            self.get_data(src + drc + results_file, data)

        means = {parameter: 0.0 for parameter in head}
        sigmas = {parameter: 0.0 for parameter in head}
        weight = np.ones(len(data["Time"])) / len(dirs)

        for parameter in data:
            d_list = np.array(data[parameter])

            mean = np.sum(d_list) / len(d_list)
            quadr_deviation = np.array([(mean - d) ** 2 for d in d_list])
            var = np.sum(quadr_deviation) / len(d_list)
            sig = np.sqrt(var)

            means[parameter] = mean
            sigmas[parameter] = sig

            fig = plt.figure(dpi=200)
            plt.hist(d_list, bins=11, weights=weight)
            plt.title(svf[:-5])
            plt.xlabel(parameter)
            plt.ylabel("Relative Häufigkeit")
            plt.figtext(0.15, 0.82, r"$\mu = $" + f"{mean:.4}")
            plt.figtext(0.15, 0.77, r"$\sigma = $" + f"{sig:.4}")
            fig.savefig(trg + parameter + ".png", dpi=fig.dpi)
            plt.close()

        jdict = {"data": data, "means": means, "sigmas": sigmas}
        with open(trg + "evaluation.json", "w") as f:
            json.dump(jdict, f, indent=2)

    def evaluate_best(self, src, trg, svf):
        data = {}
        head = []
        dirs = [d + "/" for d in sorted(listdir(src)) if isdir(src + d)][:-1]

        for drc in dirs:
            results_file = self.files_ending_with("csv", src + drc)
            if results_file is not None:
                results_file = results_file[0]
            else:
                continue

            self.get_best_data(src + drc + results_file, data)

        means = {parameter: 0.0 for parameter in head}
        sigmas = {parameter: 0.0 for parameter in head}
        weight = np.ones(len(data["Time"])) / len(dirs)

        for parameter in data:
            d_list = np.array(data[parameter])

            mean = np.sum(d_list) / len(d_list)
            quadr_deviation = np.array([(mean - d) ** 2 for d in d_list])
            var = np.sum(quadr_deviation) / len(d_list)
            sig = np.sqrt(var)

            means[parameter] = mean
            sigmas[parameter] = sig

            fig = plt.figure(dpi=200)
            plt.hist(d_list, bins=11, weights=weight)
            plt.title(svf[:-5])
            plt.xlabel(parameter)
            plt.ylabel("Relative Häufigkeit")
            plt.figtext(0.15, 0.82, r"$\mu = $" + f"{mean:.4}")
            plt.figtext(0.15, 0.77, r"$\sigma = $" + f"{sig:.4}")
            fig.savefig(trg + parameter + ".png", dpi=fig.dpi)
            plt.close()

        jdict = {"data": data, "means": means, "sigmas": sigmas}
        with open(trg + "evaluation.json", "w") as f:
            json.dump(jdict, f, indent=2)

    def files_ending_with(self, extension, src):
        files = []

        for f in listdir(src):
            if f.split(".")[-1] == extension:
                files.append(f)

        if files == []:
            return None
        else:
            return files

    def get_data(self, src_file, trg_dict):
        text = []
        with open(src_file, "r") as f:
            for line in f:
                text.append(line)

        head = text[0].split(",")
        head[-1] = head[-1][:-1]
        text = text[1:]
        results = np.loadtxt(text, delimiter=",")

        for i, parameter in enumerate(head):
            if parameter not in trg_dict:
                trg_dict[parameter] = []
            trg_dict[parameter].append(results[-1][i])

    def get_best_data(self, src_file, trg_dict):
        text = []
        with open(src_file, "r") as f:
            for line in f:
                text.append(line)

        head = text[0].split(",")
        head[-1] = head[-1][:-1]
        text = text[1:]
        results = np.loadtxt(text, delimiter=",")

        res_col = 0
        for i, h in enumerate(head):
            if h == "Residuum":
                res_col = i

        res_min_ind = 0
        res_min = results[0][res_col]

        for i, val in reversed(list(enumerate(results[:, res_col]))):
            if val < res_min:
                res_min = val
                res_min_ind = i

        for i, parameter in enumerate(head):
            if parameter not in trg_dict:
                trg_dict[parameter] = []
            trg_dict[parameter].append(results[res_min_ind][i])

    def last_csv(self):
        """Goes into the results folder and disects the last results from a
        simulation."""
        src = "data/results/"
        last_dir = sorted(listdir(src))[-1]
        src += last_dir + "/"
        csv = self.files_ending_with("csv", src)
        if csv is not None:
            csv = csv[0]
        else:
            return 0

        text = []
        head = ""
        with open(src + csv, "r") as f:
            head = f.readline()
            for line in f:
                text.append(line)

        head = head.split(",")
        head[-1] = head[-1][:-1]  # table head

        results = np.loadtxt(text[:-1], delimiter=",")

        # {paramter: array of values (table column)}
        data = {p: results[:, i] for i, p in enumerate(head)}

        return head, data

    def last_result(self):
        _, data = self.last_csv()

        plt.semilogy()
        plt.plot(np.arange(len(data["Residuum"])), data["Residuum"])
        plt.xlabel("Iterationen")
        plt.ylabel("Residuum")
        plt.show()

    def compare(self, file):
        """Give the deviations of the last found values from the set
        parameters in file."""
        head, data = self.last_csv()
        d_manager = DataManager()
        d_manager.load_settings(file)

        for p in head[:2]:
            found = data[p][-1]
            given = d_manager.parameters[p]["Value"]
            dev = (given - found) / given
            percentage = "{:.0%}".format(dev)
            print(p + ":\t" + str(percentage))


if __name__ == "__main__":
    evaluator = Evaluate()
    evaluator.evaluate_all()

    # path = "data/results/220112_132737_G/220112_135351"
    # evaluator.evaluate(path)
