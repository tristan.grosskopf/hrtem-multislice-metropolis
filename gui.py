#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 10:58:12 2021

@author: tristan
"""

import matplotlib.pyplot as plt
from os import listdir, path as ospath
from os.path import isfile, join
import numpy as np
from abtem.array import ArrayObject
from ase.visualize import view
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPushButton, QWidget, QTabWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QDoubleSpinBox, QSpinBox
from PyQt5.QtWidgets import QFrame
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QStatusBar
from PyQt5.QtWidgets import QProgressBar
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QRadioButton, QButtonGroup
from PyQt5.QtWidgets import QGraphicsView, QGraphicsPixmapItem
from PyQt5.QtWidgets import QScrollArea
from PyQt5.QtGui import QPixmap, QBrush, QColor, QFont
from PyQt5.QtWidgets import QGraphicsScene
from PyQt5 import QtCore
from PyQt5.QtCore import QThread
from annealing import SimulatedAnnealing
from annealing import HRTEM
from picmaker import PicMaker


class App(QMainWindow):
    refresh = pyqtSignal()

    def __init__(self, data):
        super().__init__()
        self.title = "HRTEM - Image Simulation"
        self.left = 0
        self.top = 0
        self.width = 900
        self.height = 0
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.data = data

        self.create_statusBar()
        self.table_widget = MyTableWidget(self, data)
        self.setCentralWidget(self.table_widget)

        self.create_menu()

        self.show()

    def create_menu(self):
        self.menu = self.menuBar().addMenu("&Menu")
        self.menu.addAction("&Save all", self.save_all)
        self.menu.addAction("&Load settings", self.load_settings)
        self.menu.addAction("&Exit", self.close)

    def create_statusBar(self):
        self.status = QStatusBar()
        self.status.showMessage("I'm the Status Bar")
        self.setStatusBar(self.status)

    def save_all(self):
        path, _ = QFileDialog.getSaveFileName(
            self, "Save File", "./data/saves", "JSON files (*.json)"
        )

        if path != "":
            if path.split(".")[-1] == "json":
                self.data.save_all(path)
            else:
                self.data.save_all(path + ".json")

    def load_settings(self):
        path, _ = QFileDialog.getOpenFileName(
            self, "Open File", "./data/saves", "JSON files (*.json)"
        )
        if path != "":
            self.data.load_settings(path)
            self.refresh.emit()


class MyTableWidget(QWidget):
    def __init__(self, parent, data):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        self.layout = QVBoxLayout(self)

        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = SelectCrystalTab(self, data)
        self.tab2 = ParametersTab(self, data)
        self.tab3 = SimulationTab(self, data)

        # Add tabs
        self.tabs.addTab(self.tab1, "Crystal")
        self.tabs.addTab(self.tab2, "Parameters")
        self.tabs.addTab(self.tab3, "Simulation")

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

        # Connect slots and signals
        func = self.tab2.update_layout
        self.tab1.update_parameter_layout.connect(func)


class SelectCrystalTab(QWidget):
    update_parameter_layout = pyqtSignal()

    def __init__(self, parent, data):
        super(QWidget, self).__init__(parent)

        parent.parent.refresh.connect(self.refresh)

        self.data = data
        self.radio_btns = []
        self.markers = {}
        self.current_angle = 0

        self.setup_layout()

    def refresh(self):
        model = self.data.selected_model
        name = model["Name"] + " - " + model["Formula"]
        index = self.select_crystal.findText(name)

        if index != -1:
            self.select_crystal.setCurrentIndex(index)

        self.set_model()

        if self.data.recording_path != "":
            self.load_image(path=self.data.recording_path)
        else:
            self.mark_positions_selector()

    def setup_layout(self):
        self.super_layout = QHBoxLayout()
        self.super_layout.addSpacing(20)

        self.main_layout = QVBoxLayout()
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.main_layout.addStretch(0)
        self.main_layout.addSpacing(20)
        self.main_layout.addWidget(QLabel("Select a crystal model:"))
        self.select_crystal = QComboBox()
        for name in self.data.crystal_models:
            formula = self.data.crystal_models[name]["Formula"]
            self.select_crystal.addItem(name + " - " + formula)
        self.select_crystal.currentIndexChanged.connect(self.set_model)
        self.main_layout.addWidget(self.select_crystal)

        self.main_layout.addSpacing(20)

        self.create_btn = QPushButton("&Create new model")
        self.create_btn.clicked.connect(self.open_crystal_dialogue)
        self.main_layout.addWidget(self.create_btn)

        self.edit_btn = QPushButton("Edit selected model")
        self.edit_btn.clicked.connect(self.open_edit_dialogue)
        self.main_layout.addWidget(self.edit_btn)

        self.delete_btn = QPushButton("Delete selected model")
        self.main_layout.addWidget(self.delete_btn)

        self.main_layout.addSpacing(20)

        self.show_layout = QHBoxLayout()
        self.show_btn = QPushButton("&Show\nCrystal")
        self.show_btn.setFixedSize(85, 85)
        self.show_btn.clicked.connect(self.show_crystal)
        self.show_layout.addWidget(self.show_btn)
        self.main_layout.addLayout(self.show_layout)

        self.main_layout.addSpacing(20)

        self.super_layout.addLayout(self.main_layout)
        self.super_layout.addSpacing(20)

        self.position_selectors_layout = QGridLayout()
        if self.select_crystal.currentIndex() != -1:
            self.set_model()

        self.separator = QFrame()
        self.separator.setFrameShape(QFrame.VLine)
        self.separator.setFrameShadow(QFrame.Raised)
        self.super_layout.addWidget(self.separator)

        self.super_layout.addLayout(self.position_selectors_layout)

        ######################################################################

        self.image_layout = QVBoxLayout()
        self.recording_control_layout = QHBoxLayout()

        self.viewer = PhotoViewer(self)
        self.viewer.photoClicked.connect(self.photo_clicked)
        self.image_layout.addWidget(self.viewer)

        self.btn_file = QPushButton("Select Recording")
        self.btn_file.clicked.connect(self.load_image)
        self.recording_control_layout.addWidget(self.btn_file)

        self.recording_control_layout.addStretch(0)

        self.angle_label = QLabel("Total rotation angle:")
        self.recording_control_layout.addWidget(self.angle_label)
        self.total_angle_label = QLabel("0")
        rec_path = self.data.recording_path
        if self.data.recording_path != "":
            val_str = str(self.data.rotation_angles[rec_path])
            self.total_angle_label.setText(val_str)
        self.recording_control_layout.addWidget(self.total_angle_label)

        self.recording_control_layout.addSpacing(20)

        rot_text = QLabel("Rotate by amount [°]:")
        self.recording_control_layout.addWidget(rot_text)
        self.recording_control_layout.addSpacing(10)
        self.angle_control = QDoubleSpinBox()
        self.angle_control.setMaximum(180)
        self.angle_control.setMinimum(-180)
        self.angle_control.setDecimals(2)
        self.angle_control.setSingleStep(0.05)
        self.angle_control.setEnabled(False)
        self.recording_control_layout.addWidget(self.angle_control)

        self.image_layout.addLayout(self.recording_control_layout)

        self.super_layout.addLayout(self.image_layout)

        self.setLayout(self.super_layout)

        self.main_layout.addStretch(0)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Return:
            self.rotate()

    def open_crystal_dialogue(self):
        self.dlg = NewCrystalDialogue(self)
        self.dlg.accepted.connect(self.add_model)
        self.dlg.setModal(True)
        self.dlg.exec_()

    def add_model(self):
        name, formula = self.dlg.get_value()
        self.data.create_crystal_model(name, formula)
        self.select_crystal.addItem(name + " - " + formula)
        self.select_crystal.setCurrentIndex(self.select_crystal.count() - 1)

    def open_edit_dialogue(self):
        if self.select_crystal.currentIndex() != -1:
            dlg = EditCrystalDialogue(self, self.data)
            dlg.setModal(True)
            dlg.exec_()
            self.update_parameter_layout.emit()

    def set_model(self):
        entry = self.select_crystal.currentText()
        model_name = entry.split(" - ")[0]
        self.data.selected_model = self.data.crystal_models[model_name]
        self.data.update_module_func()
        self.update_parameter_layout.emit()

        self.mark_positions_selector()

    def show_crystal(self):
        model = self.data.selected_model
        wyckoff = model["Atoms"]
        atoms = list(wyckoff.keys())
        vectors = [wyckoff[atom] for atom in atoms]
        lattice_constant = model["Lattice Constant"]
        formula = model["Formula"]

        hrtem = HRTEM(formula, vectors, lattice_constant)
        hrtem.show_crystal()

    def load_image(self, path=None):
        if path == None or path == False:
            path, _ = QFileDialog.getOpenFileName(
                self, "Open file", "data/recordings/", "Recording files (*.dm4 *.hdf5)"
            )

        if path != "" and path != None:
            self.data.read_recording(path)
            self.viewer.setPhoto(QPixmap("/tmp/hrtem/recording.png"))
            angle = self.data.rotation_angle
            self.current_angle = angle
            self.mark_positions_selector()

            self.angle_control.setEnabled(True)

            self.angle_control.setValue(0)
            self.total_angle_label.setText(str(angle))

    def photo_clicked(self, pos):
        for r_btn in self.radio_btns:
            if r_btn.isChecked():
                shapex, shapey = self.data.rotated_intensity.shape
                x = pos.x()
                y = shapex - pos.y()
                tup = (x, y)
                atom = r_btn.text()
                self.data.set_estimated_position(atom, tup)
                self.set_marker(atom)

    def mark_positions_selector(self):
        layout = self.position_selectors_layout

        self.radio_btns.clear()

        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)

        atoms = list(self.data.selected_model["Atoms"].keys())

        for i, atom in enumerate(atoms):
            self.radio_btns.append(QRadioButton(atom))
            layout.addWidget(self.radio_btns[-1], i, 0)

        self.refresh_markers()

    def set_marker(self, atom):
        if atom in self.markers:
            for item in self.markers[atom]:
                self.viewer.scene().removeItem(item)

        shapex, shapey = self.data.rotated_intensity.shape
        x, y_prime = self.data.get_estimated_position(atom)
        y = shapex - y_prime

        half_line = int(0.025 * shapex)
        line1 = self.viewer.scene().addLine(x, y - half_line, x, y + half_line)
        line1.setTransformOriginPoint(x, y)
        line2 = self.viewer.scene().addLine(x - half_line, y, x + half_line, y)
        line2.setTransformOriginPoint(x, y)
        font = QFont()
        size = 0.7 * half_line
        font.setPointSize(size)
        label = self.viewer.scene().addText(atom)
        label.setFont(font)
        label.setPos(x - 3 * size, y - 3 * size)
        label.setTransformOriginPoint(3 * size, 3 * size)

        self.markers[atom] = (line1, line2, label)

    def refresh_markers(self):
        for atom in self.markers:
            for item in self.markers[atom]:
                self.viewer.scene().removeItem(item)

        self.markers.clear()

        atoms = list(self.data.selected_model["Atoms"].keys())
        for atom in atoms:
            pos = self.data.get_estimated_position(atom)
            if pos != [0.0, 0.0]:
                self.set_marker(atom)
            else:
                self.markers[atom] = ()

    def rotate(self, amount=None):
        if amount == None:
            amount = self.angle_control.value()
        self.data.rotation_angle += amount
        self.data.rotation_angle %= 360
        tot_angle = self.data.rotation_angle
        self.current_angle = tot_angle
        self.total_angle_label.setText(str(tot_angle))

        self.data.rotate_recording()
        self.data.make_picture()
        self.viewer._photo.setPixmap(QPixmap("/tmp/hrtem/recording.png"))


class ParametersTab(QWidget):
    def __init__(self, parent, data):
        super(QWidget, self).__init__(parent)

        parent.parent.refresh.connect(self.refresh)

        self.data = data
        self.setStyleSheet("QWidget { background-color: white }")

        self.main_layout = QVBoxLayout()
        self.table_layout = QHBoxLayout()

        self.std_widget_dict = {}
        parameter_names = SimulatedAnnealing.parameter_names
        aberration_coefficients = SimulatedAnnealing.aberration_coefficients
        std_param_list = parameter_names + aberration_coefficients
        self.std_layout = QHBoxLayout()
        self.std_btn_group = []

        self.setup_table_layout(
            self.std_widget_dict,
            std_param_list,
            self.std_layout,
            self.update_parameter,
            False,
            self.std_btn_group,
        )
        self.fill_layout(self.std_layout, self.std_widget_dict, self.data.parameters)

        self.usr_widget_dict = {}
        self.usr_btn_group = []
        usr_param_list = []
        model = self.data.selected_model
        self.usr_layout = QHBoxLayout()
        if "Code" in model:
            code = model["Code"]
            check = self.data.selected_model["Custom"] == True
            if code in self.data.usr_parameters and check:
                usr_param_list = list(self.data.usr_parameters[code])
                self.setup_table_layout(
                    self.usr_widget_dict,
                    usr_param_list,
                    self.usr_layout,
                    self.update_parameter,
                    True,
                    self.usr_btn_group,
                )
                self.fill_layout(
                    self.usr_layout,
                    self.usr_widget_dict,
                    self.data.usr_parameters[code],
                )

        container = QWidget()
        container.setLayout(self.main_layout)
        scroll = QScrollArea()
        scroll.setWidgetResizable(True)
        scroll.setWidget(container)
        scroll_layout = QHBoxLayout()
        scroll_layout
        scroll_layout.addWidget(scroll)
        self.setLayout(scroll_layout)

        self.table_layout.addLayout(self.std_layout)
        self.table_layout.addSpacing(30)
        self.table_layout.addLayout(self.usr_layout)
        self.main_layout.addLayout(self.table_layout)

    def refresh(self):
        self.fill_layout(self.std_layout, self.std_widget_dict, self.data.parameters)
        self.data.update_module_func()
        self.update_layout()

    def setup_table_layout(
        self, widget_dict, param_list, layout, update, discrete, btn_groups
    ):
        if param_list == []:
            return 0

        layout.setAlignment(Qt.AlignTop)

        height = 25

        grid = QGridLayout()
        val_label = QLabel("Start Value")
        val_label.setFixedHeight(height)
        min_label = QLabel("Min")
        min_label.setFixedHeight(height)
        max_label = QLabel("Max")
        max_label.setFixedHeight(height)
        grid.addWidget(val_label, 0, 1)
        grid.addWidget(min_label, 0, 2)
        grid.addWidget(max_label, 0, 3)
        layout.addLayout(grid)

        layout.addSpacing(30)

        radios = QVBoxLayout()
        radios.setAlignment(Qt.AlignTop)
        radio_head = QHBoxLayout()
        find_label = QLabel("Find")
        find_label.setFixedHeight(height)
        fixed_label = QLabel("Fixed")
        fixed_label.setFixedHeight(height)
        radio_head.addWidget(find_label)
        radio_head.addWidget(fixed_label)
        radios.addLayout(radio_head)
        layout.addLayout(radios)

        checks = QVBoxLayout()
        if discrete:
            layout.addSpacing(30)
            discrete_head = QLabel("Discrete")
            discrete_head.setFixedHeight(height)
            checks.addWidget(discrete_head)
            layout.addLayout(checks)

        props = ["Value", "Min", "Max"]

        for i, param in enumerate(param_list):
            widget_dict[param] = {}

            p_label = QLabel(param)
            p_label.setFixedWidth(150)
            p_label.setFixedHeight(height)
            grid.addWidget(p_label, i + 1, 0)

            for j, prop in enumerate(props):
                line = QLineEdit()
                line.setFixedWidth(150)
                line.setFixedHeight(height)
                line.id = (param, prop)
                grid.addWidget(line, i + 1, j + 1)
                line.editingFinished.connect(update)
                widget_dict[param][prop] = line

            fix_radio = QRadioButton()
            fix_radio.id = (param, "Fixed")
            fix_radio.setFixedWidth(40)
            fix_radio.setFixedHeight(height)
            fix_radio.toggled.connect(update)
            find_radio = QRadioButton()
            find_radio.id = (param, "Find")
            find_radio.setFixedWidth(40)
            find_radio.setFixedHeight(height)
            find_radio.toggled.connect(update)
            btn_group = QButtonGroup()
            btn_group.addButton(fix_radio)
            btn_group.addButton(find_radio)
            btn_groups.append(btn_group)

            radio_row = QHBoxLayout()
            radio_row.addWidget(find_radio)
            radio_row.addWidget(fix_radio)
            radios.addLayout(radio_row)

            widget_dict[param]["Find"] = find_radio
            widget_dict[param]["Fixed"] = fix_radio

            if discrete:
                discrete_check = QCheckBox()
                discrete_check.setFixedHeight(height)
                discrete_check.id = (param, "Discrete")
                discrete_check.stateChanged.connect(update)
                widget_dict[param]["Discrete"] = discrete_check
                checks.addWidget(discrete_check)

    def fill_layout(self, layout, widget_dict, parameters):
        ps = parameters

        for param in ps:
            widget_dict[param]["Value"].setText(str(ps[param]["Value"]))
            widget_dict[param]["Min"].setText(str(ps[param]["Min"]))
            widget_dict[param]["Max"].setText(str(ps[param]["Max"]))

            if ps[param]["Fixed"] == 0:
                widget_dict[param]["Find"].setChecked(True)
            else:
                widget_dict[param]["Fixed"].setChecked(True)

            if "Discrete" in widget_dict[param]:
                if ps[param]["Discrete"] == 0:
                    widget_dict[param]["Discrete"].setChecked(False)
                else:
                    widget_dict[param]["Discrete"].setChecked(True)

    def update_parameter(self):
        (param, prop) = self.sender().id

        if prop in ["Value", "Min", "Max"]:
            val = float(self.sender().text())
            self.data.set_parameter(prop, parameter=param, value=val)
        elif prop in ["Fixed", "Discrete"]:
            if self.sender().isChecked():
                self.data.set_parameter(prop, parameter=param, value=1)
            else:
                self.data.set_parameter(prop, parameter=param, value=0)

    def remove_widgets(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setParent(None)
                else:
                    self.remove_widgets(item.layout())

    def update_layout(self):
        self.usr_widget_dict.clear()
        self.usr_btn_group.clear()

        self.remove_widgets(self.usr_layout)

        model = self.data.selected_model
        if "Code" in model:
            code = model["Code"]
            check = self.data.selected_model["Custom"]
            if code in self.data.usr_parameters and check:
                usr_param_list = list(self.data.usr_parameters[code])
                self.setup_table_layout(
                    self.usr_widget_dict,
                    usr_param_list,
                    self.usr_layout,
                    self.usr_update_parameter,
                    True,
                    self.usr_btn_group,
                )
                self.fill_layout(
                    self.usr_layout,
                    self.usr_widget_dict,
                    self.data.usr_parameters[code],
                )


class SimulationTab(QWidget):
    def __init__(self, parent, data):
        super(QWidget, self).__init__(parent)

        parent.parent.refresh.connect(self.refresh)
        self.parent = parent

        self.data = data

        self.main_layout = QVBoxLayout()
        self.view_layout = QHBoxLayout()

        self.simulate_layout = QVBoxLayout()
        self.setup_simulate_layout(self.simulate_layout)
        self.view_layout.addLayout(self.simulate_layout)

        separator1 = QFrame()
        separator1.setFrameShape(QFrame.VLine)
        separator1.setFrameShadow(QFrame.Raised)
        self.view_layout.addWidget(separator1)

        self.show_layout = QVBoxLayout()
        self.setup_viewer_layout(self.show_layout)
        self.view_layout.addLayout(self.show_layout)

        separator2 = QFrame()
        separator2.setFrameShape(QFrame.VLine)
        separator2.setFrameShadow(QFrame.Raised)
        self.view_layout.addWidget(separator2)

        self.picmaker_layout = QVBoxLayout()
        self.setup_picmaker_layout(self.picmaker_layout)
        self.view_layout.addLayout(self.picmaker_layout)

        self.main_layout.addLayout(self.view_layout)

        self.progress_bar = QProgressBar()
        self.progress_bar.setAlignment(Qt.AlignBottom)
        self.main_layout.addWidget(self.progress_bar)

        self.setLayout(self.main_layout)

    def setup_simulate_layout(self, layout):
        layout.addSpacing(10)

        head_layout = QHBoxLayout()
        head_layout.setAlignment(Qt.AlignTop)
        head_label = QLabel("Simulated Annealing")
        head_label.setStyleSheet("font-weight: bold")
        head_label.setAlignment(Qt.AlignCenter)
        head_layout.addWidget(head_label)
        layout.addLayout(head_layout)

        layout.addStretch(0)
        layout.addSpacing(20)

        layout.addWidget(QLabel("Duration:"))
        self.iterations_box = QSpinBox()
        self.iterations_box.setValue(10)
        self.iterations_box.setMaximum(1e6)
        self.iterations_box.setSingleStep(50)
        self.iterations_box.editingFinished.connect(self.update_iterations)
        layout.addWidget(self.iterations_box)

        layout.addSpacing(20)

        self.check_multiple = QCheckBox("Multiple runs")
        self.check_multiple.clicked.connect(self.multiple)
        layout.addWidget(self.check_multiple)
        self.runs_box = QSpinBox()
        self.runs_box.setEnabled(False)
        self.runs_box.editingFinished.connect(self.runs)
        self.runs_box.setValue(1)
        self.runs_box.setMaximum(1e6)
        self.runs_box.setSingleStep(50)
        layout.addWidget(self.runs_box)

        layout.addSpacing(20)

        self.check_schedule = QCheckBox("Run multiple save files")
        self.check_schedule.clicked.connect(self.schedule)
        layout.addWidget(self.check_schedule)
        self.select_schedule = QPushButton("Select directory")
        self.select_schedule.setEnabled(False)
        self.select_schedule.clicked.connect(self.load_schedule)
        layout.addWidget(self.select_schedule)
        self.dir_label = QLabel("")
        layout.addWidget(self.dir_label)

        layout.addSpacing(20)

        options_btn = QPushButton("More options")
        options_btn.clicked.connect(self.open_options_dialogue)
        layout.addWidget(options_btn)

        layout.addSpacing(20)

        start_layout = QHBoxLayout()
        self.start_btn = QPushButton("Start\nSimulating")
        self.start_btn.setFixedSize(85, 85)
        self.start_btn.clicked.connect(self.start_simulate)
        start_layout.addWidget(self.start_btn)
        layout.addLayout(start_layout)

        layout.addSpacing(20)
        layout.addStretch(0)

    def setup_viewer_layout(self, layout):
        self.viewer = PhotoViewer(self)
        layout.addWidget(self.viewer)

        self.save_btn = QPushButton("Save Image")
        self.save_btn.clicked.connect(self.save_image)
        layout.addWidget(self.save_btn)

    def setup_picmaker_layout(self, layout):
        layout.addSpacing(10)

        head_layout = QHBoxLayout()
        head_layout.setAlignment(Qt.AlignTop)
        head_label = QLabel("Simulate a Single\nImage")
        head_label.setStyleSheet("font-weight: bold")
        head_label.setAlignment(Qt.AlignCenter)
        head_layout.addWidget(head_label)
        layout.addLayout(head_layout)

        layout.addStretch(0)
        layout.addSpacing(20)

        self.poisson_check = QCheckBox("Add Poisson Noise")
        self.poisson_check.clicked.connect(self.poisson)
        layout.addWidget(self.poisson_check)
        self.gaussian_check = QCheckBox("Add Gaussian Noise")
        self.gaussian_check.clicked.connect(self.gaussian)
        layout.addWidget(self.gaussian_check)

        layout.addSpacing(10)

        variance_label = QLabel("Variance:")
        layout.addWidget(variance_label)
        self.variance_box = QSpinBox()
        self.variance_box.setMaximum(1e6)
        self.variance_box.setEnabled(False)
        self.variance_box.editingFinished.connect(self.variance)
        layout.addWidget(self.variance_box)

        layout.addSpacing(10)

        electrons_label = QLabel("Electrons per pixel:")
        layout.addWidget(electrons_label)
        self.electrons_box = QSpinBox()
        self.electrons_box.setMaximum(1e6)
        self.electrons_box.setEnabled(False)
        self.electrons_box.editingFinished.connect(self.electrons)
        layout.addWidget(self.electrons_box)

        layout.addSpacing(20)

        make_layout = QHBoxLayout()
        self.make_btn = QPushButton("Make\nImage")
        self.make_btn.setFixedSize(85, 85)
        self.make_btn.clicked.connect(self.make_image)
        make_layout.addWidget(self.make_btn)
        layout.addLayout(make_layout)

        layout.addStretch(0)

    def refresh(self):
        self.iterations_box.setValue(self.data.iterations)

        multiple = self.data.multiple_runs
        self.check_multiple.setChecked(multiple)
        self.runs_box.setEnabled(multiple)
        self.runs_box.setValue(self.data.runs)

        schedule = self.data.schedule
        self.check_schedule.setChecked(schedule)
        self.select_schedule.setEnabled(schedule)
        self.dir_label.setText(self.data.schedule_path)

        self.poisson_check.setChecked(self.data.poisson)
        self.gaussian_check.setChecked(self.data.gaussian)

        if self.data.poisson or self.data.gaussian:
            self.electrons_box.setEnabled(True)
        else:
            self.electrons_box.setEnabled(False)

        self.variance_box.setEnabled(self.data.gaussian)
        self.variance_box.setValue(self.data.variance)
        self.electrons_box.setValue(self.data.electrons)

    def update_iterations(self):
        self.data.iterations = self.iterations_box.value()

    def multiple(self):
        if self.sender().isChecked():
            self.runs_box.setEnabled(True)
            self.data.multiple_runs = True
        else:
            self.runs_box.setEnabled(False)
            self.data.multiple_runs = False

    def runs(self):
        self.data.runs = self.sender().value()

    def schedule(self):
        if self.sender().isChecked():
            self.select_schedule.setEnabled(True)
            if self.data.schedule_path != "":
                self.data.schedule = True
        else:
            self.select_schedule.setEnabled(False)
            self.data.schedule = False

    def load_schedule(self):
        func = QFileDialog.getExistingDirectory
        path = str(func(self, "Open Folder", "data/")) + "/"

        if path != "":
            path = ospath.relpath(path)
            self.dir_label.setText(path)
            self.data.schedule = True
            self.data.schedule_path = path

    def poisson(self):
        check = self.sender().isChecked()
        self.data.poisson = check
        if check or self.data.gaussian:
            self.electrons_box.setEnabled(True)
        else:
            self.electrons_box.setEnabled(False)

    def gaussian(self):
        check = self.sender().isChecked()
        self.data.gaussian = check
        if check or self.data.poisson:
            self.electrons_box.setEnabled(True)
        else:
            self.electrons_box.setEnabled(False)

        self.variance_box.setEnabled(check)

    def variance(self):
        self.data.variance = self.sender().value()

    def electrons(self):
        self.data.electrons = self.sender().value()

    def make_image(self):
        picmaker = PicMaker(self.data)
        picmaker.simulate_image()

        recording = self.data.simulated_intensity

        plot_ar = recording - np.min(recording)
        plot_ar = plot_ar / np.max(plot_ar)

        (x_sample, y_sample) = plot_ar.shape
        img = np.zeros((x_sample, y_sample, 3))
        for i in range(x_sample):
            for j in range(y_sample):
                v = plot_ar[i, j]
                img[i, j] = (v, v, v)

        plt.imsave("/tmp/hrtem/result.png", img)

        self.viewer.setPhoto(QPixmap("/tmp/hrtem/result.png"))

    def save_image(self):
        intensity = self.data.simulated_intensity

        if intensity is not None:
            path, _ = QFileDialog.getSaveFileName(
                self,
                "Save File",
                "data/recordings/",
                "Recording files (*.dm4 *.hdf5 *.png)",
            )
            if path != None:
                measure = ArrayObject(intensity)
                measure = measure.to_hyperspy()
                suf = path.split(".")[-1]
                if suf != "hdf5" and suf != "png":
                    measure.save(path + ".hdf5")
                elif suf == "hdf5":
                    measure.save(path)
                elif suf == "png":
                    plot_ar = intensity - np.min(intensity)
                    plot_ar = plot_ar / np.max(plot_ar)

                    (x_sample, y_sample) = plot_ar.shape
                    img = np.zeros((x_sample, y_sample, 3))
                    for i in range(x_sample):
                        for j in range(y_sample):
                            v = plot_ar[i, j]
                            img[i, j] = (v, v, v)

                    plt.imsave(path, img)

    def open_options_dialogue(self):
        self.dlg = MoreOptionsDialogue(self)
        self.dlg.accepted.connect(self.save_options)
        self.dlg.setModal(True)
        self.dlg.exec_()

    def save_options(self):
        values = self.dlg.get_value()

        self.data.slice_thickness = values[0]
        self.data.lateral_sampling = values[1]
        self.data.wiggle_factor = values[2]
        self.data.annealing_constant = values[3]

    def start_simulate(self):
        if self.data.schedule:
            self.simulate_schedule()
        else:
            self.simulate()

    def simulate(self):
        duration = self.data.iterations

        self.progress_bar.reset()
        self.progress_bar.setMaximum(duration - 1)
        self.progress_bar.setMinimum(0)

        self.thread = QThread()
        self.annealingObject = SimulatedAnnealing(self.data.copy())
        self.annealingObject.moveToThread(self.thread)
        self.annealingObject.finished.connect(self.finished)
        self.annealingObject.progress.connect(self.report_progress)
        self.annealingObject.plotInfo.connect(self.plot_result)

        self.thread.start()
        self.annealingObject.simulate()

    def simulate_schedule(self):
        status = self.parent.parent.status

        curr_parameters = self.data.save_file_path

        path = self.data.schedule_path
        files = [f for f in listdir(path) if isfile(join(path, f))]

        for i, f in enumerate(files):
            done = f + " (" + str(i + 1) + "/" + str(len(files)) + ")"
            status.showMessage(done)
            self.data.load_settings(path + "/" + f)
            self.simulate()

        self.data.load_settings(curr_parameters)

    def finished(self):
        self.annealingObject.deleteLater()
        self.thread.quit()
        self.thread.deleteLater()

    def report_progress(self, iteration):
        self.progress_bar.setValue(iteration)

    def plot_result(self, ar, path):
        self.data.simulated_intensity = ar.copy()
        self.viewer.setPhoto(QPixmap(path + "result.png"))


class NewCrystalDialogue(QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        self.data = parent.data
        self.setWindowTitle("Create a new crystal model")
        self.dlgLayout = QVBoxLayout()

        self.enter_name = QLineEdit()
        self.enter_name.returnPressed.connect(self.finished)
        self.enter_name.setPlaceholderText("Name")
        self.dlgLayout.addWidget(self.enter_name)

        self.enter_formula = QLineEdit()
        self.enter_formula.returnPressed.connect(self.finished)
        self.enter_formula.setPlaceholderText("Chemical formula")
        self.dlgLayout.addWidget(self.enter_formula)

        self.status_label = QLabel("")
        self.dlgLayout.addWidget(self.status_label)

        self.accept_btn = QPushButton("Submit")
        self.accept_btn.clicked.connect(self.finished)
        self.dlgLayout.addWidget(self.accept_btn)

        self.setLayout(self.dlgLayout)

    def get_value(self):
        return (self.enter_name.text(), self.enter_formula.text())

    def finished(self):
        if self.enter_name.text() == "":
            self.status_label.setText("Please enter a name for the model.")
        elif self.enter_formula.text() == "":
            self.status_label.setText("Please enter a chemical formula.")
        elif self.data.parse_formula(self.enter_formula.text()) == []:
            self.status_label.setText("The formula couldn't be parsed.")
        elif self.enter_name.text() in self.data.crystal_models:
            self.status_label.setText("This name already exists.")
        else:
            self.accept()


class EditCrystalDialogue(QDialog):
    def __init__(self, parent, data):
        super().__init__(parent)
        self.setWindowTitle("Edit crystal model")

        self.data = data

        self.main_layout = QVBoxLayout()
        self.setup_layout(self.main_layout)
        self.setLayout(self.main_layout)

    def setup_layout(self, layout):
        layout.addWidget(QLabel(self.data.selected_model["Formula"]))

        self.setup_position_grid(layout)
        self.setup_dimension_layout(layout)

        self.setup_custom_layout(layout)

        self.btn_layout = QHBoxLayout()
        layout.addLayout(self.btn_layout)

        self.save_btn = QPushButton("Save")
        self.save_btn.clicked.connect(self.save_data)
        self.btn_layout.addWidget(self.save_btn)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.finished)
        self.btn_layout.addWidget(self.cancel_button)

        if self.data.selected_model["Custom"]:
            self.check_custom.setChecked(True)
        else:
            self.check_custom.setChecked(True)
            self.check_custom.setChecked(False)

    def setup_position_grid(self, layout):
        atoms = list(self.data.selected_model["Atoms"].keys())

        grid = QGridLayout()
        layout.addLayout(grid)

        self.spinboxes = []

        grid.addWidget(QLabel("x"), 0, 1)
        grid.addWidget(QLabel("y"), 0, 2)
        grid.addWidget(QLabel("z"), 0, 3)

        for i, atom in enumerate(atoms):
            grid.addWidget(QLabel(atom), i + 1, 0)
            new_spinboxes = [QDoubleSpinBox() for i in range(3)]
            self.spinboxes.append(new_spinboxes)
            grid.addWidget(self.spinboxes[-1][0], i + 1, 1)
            grid.addWidget(self.spinboxes[-1][1], i + 1, 2)
            grid.addWidget(self.spinboxes[-1][2], i + 1, 3)
            positions = self.data.selected_model["Atoms"][atom]
            self.spinboxes[-1][0].setValue(positions[0])
            self.spinboxes[-1][1].setValue(positions[1])
            self.spinboxes[-1][2].setValue(positions[2])

        for spinbs in self.spinboxes:
            for spin in spinbs:
                spin.setSingleStep(0.5)

        lat_const_layout = QHBoxLayout()
        layout.addLayout(lat_const_layout)

        lat_const_layout.addWidget(QLabel("Lattice constant [Å]"))
        self.lattice_constant = QDoubleSpinBox()
        self.lattice_constant.setSingleStep(0.5)
        self.lattice_constant.setDecimals(5)
        a = self.data.selected_model["Lattice Constant"]
        self.lattice_constant.setValue(a)
        lat_const_layout.addWidget(self.lattice_constant)

    def setup_dimension_layout(self, layout):
        labels = ["x", "y", "z"]
        self.dimension_line_edits = [QSpinBox() for i in range(3)]

        dimensions_layout = QVBoxLayout()
        layout.addLayout(dimensions_layout)
        dimensions_layout.addWidget(QLabel("Dimensions"))

        for i in range(3):
            self.dimension_line_edits[i].name = labels[i]
            self.dimension_line_edits[i].setFixedWidth(50)
            dim = self.data.selected_model["Dimensions"][labels[i]]
            self.dimension_line_edits[i].setMinimum(1)
            self.dimension_line_edits[i].setMaximum(999)
            self.dimension_line_edits[i].setValue(dim)
            new_layout = QHBoxLayout()
            new_label = QLabel(labels[i])
            new_label.setFixedWidth(20)
            new_layout.addWidget(new_label)
            new_layout.addWidget(self.dimension_line_edits[i])

            dimensions_layout.addLayout(new_layout)
            dimensions_layout.setAlignment(Qt.AlignLeft)

    def setup_custom_layout(self, layout):
        msg = "Use a user-defined module to model the crystal."
        self.check_custom = QCheckBox(msg)
        self.check_custom.stateChanged.connect(self.toggle_custom)
        layout.addWidget(self.check_custom)

        self.load_btn = QPushButton("&Load Module")
        self.load_btn.clicked.connect(self.load_module)
        layout.addWidget(self.load_btn)

        self.path_label = QLabel(self.data.selected_model["Code"])
        layout.addWidget(self.path_label)

    def load_module(self):
        path, _ = QFileDialog.getOpenFileName(
            self, "Open file", "data/", "Python script (*.py)"
        )

        new_path = ospath.relpath(path)
        self.path_label.setText(new_path)

    def toggle_custom(self):
        enable = False

        if self.sender().isChecked():
            enable = True

        self.load_btn.setEnabled(enable)

    def save_data(self):
        model = self.data.selected_model
        atoms = list(model["Atoms"].keys())

        if self.check_custom.isChecked():
            model["Custom"] = True
        else:
            model["Custom"] = False

        for i, atom in enumerate(atoms):
            positions = [box.value() for box in self.spinboxes[i]]
            model["Atoms"][atom] = positions

        model["Lattice Constant"] = self.lattice_constant.value()

        dims = [dim.value() for dim in self.dimension_line_edits]
        model["Dimensions"] = {"x": dims[0], "y": dims[1], "z": dims[2]}

        model["Code"] = self.path_label.text()

        self.data.save_model()

        self.finished()

    def finished(self):
        self.accept()


class MoreOptionsDialogue(QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        self.data = parent.data
        self.setWindowTitle("More Options")
        self.dlg_layout = QVBoxLayout()

        thick_label = QLabel("Slice thickness:")
        self.dlg_layout.addWidget(thick_label)
        self.thick_box = QDoubleSpinBox()
        self.thick_box.setDecimals(4)
        self.thick_box.setSingleStep(0.1)
        self.thick_box.setValue(self.data.slice_thickness)
        self.dlg_layout.addWidget(self.thick_box)

        self.dlg_layout.addSpacing(20)

        sampling_label = QLabel("Lateral sampling:")
        self.dlg_layout.addWidget(sampling_label)
        self.sampling_box = QDoubleSpinBox()
        self.sampling_box.setDecimals(4)
        self.sampling_box.setSingleStep(0.01)
        self.sampling_box.setValue(self.data.lateral_sampling)
        self.dlg_layout.addWidget(self.sampling_box)

        self.dlg_layout.addSpacing(20)

        wiggle_label = QLabel("Wiggle (in unit cells):")
        self.dlg_layout.addWidget(wiggle_label)
        self.wiggle_box = QDoubleSpinBox()
        self.wiggle_box.setDecimals(2)
        self.wiggle_box.setSingleStep(0.25)
        self.wiggle_box.setValue(self.data.wiggle_factor)
        self.dlg_layout.addWidget(self.wiggle_box)

        self.dlg_layout.addSpacing(20)

        anneal_label = QLabel("Annealing constant:")
        self.dlg_layout.addWidget(anneal_label)
        self.anneal_box = QDoubleSpinBox()
        self.anneal_box.setDecimals(1)
        self.anneal_box.setSingleStep(0.1)
        self.anneal_box.setValue(self.data.annealing_constant)
        self.dlg_layout.addWidget(self.anneal_box)

        self.dlg_layout.addSpacing(20)

        self.btn_layout = QHBoxLayout()
        self.dlg_layout.addLayout(self.btn_layout)

        self.save_btn = QPushButton("Save")
        self.save_btn.clicked.connect(self.finished)
        self.btn_layout.addWidget(self.save_btn)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.reject)
        self.btn_layout.addWidget(self.cancel_button)

        self.setLayout(self.dlg_layout)

    def get_value(self):
        return (
            self.thick_box.value(),
            self.sampling_box.value(),
            self.wiggle_box.value(),
            self.anneal_box.value(),
        )

    def finished(self):
        self.accept()


class PhotoViewer(QGraphicsView):
    photoClicked = QtCore.pyqtSignal(QtCore.QPoint)

    def __init__(self, parent):
        super(PhotoViewer, self).__init__(parent)
        self._zoom = 0
        self._empty = True
        self._scene = QGraphicsScene(self)
        self._photo = QGraphicsPixmapItem()
        self._scene.addItem(self._photo)
        self.setScene(self._scene)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        # self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        # self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setBackgroundBrush(QBrush(QColor(50, 50, 50)))
        self.setFrameShape(QFrame.NoFrame)

    def hasPhoto(self):
        return not self._empty

    def fitInView(self, scale=True):
        rect = QtCore.QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.hasPhoto():
                unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(
                    viewrect.width() / scenerect.width(),
                    viewrect.height() / scenerect.height(),
                )
                self.scale(factor, factor)
            self._zoom = 0

    def setPhoto(self, pixmap=None):
        self._zoom = 0
        if pixmap and not pixmap.isNull():
            self._empty = False
            self.setDragMode(QGraphicsView.NoDrag)
            self._photo.setPixmap(pixmap)
        else:
            self._empty = True
            self.setDragMode(QGraphicsView.NoDrag)
            self._photo.setPixmap(QPixmap())
        self.fitInView()

    def wheelEvent(self, event):
        if self.hasPhoto():
            if event.angleDelta().y() > 0:
                factor = 1.25
                self._zoom += 1
            else:
                factor = 0.8
                self._zoom -= 1
            if self._zoom > 0:
                self.scale(factor, factor)
            elif self._zoom == 0:
                self.fitInView()
            else:
                self._zoom = 0

    def mousePressEvent(self, event):
        if self._photo.isUnderMouse():
            self.photoClicked.emit(self.mapToScene(event.pos()).toPoint())
        super(PhotoViewer, self).mousePressEvent(event)
