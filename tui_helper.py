#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on [2023-08-15 Tue 14:23].

@author: tristan
"""

import numpy as np
import matplotlib.pyplot as plt
import json
from hrtem import HRTEM
from os.path import isfile
from os import remove
from picmaker import PicMaker
from log import log
from dmanager import DataManager


def generate_save_file(data: DataManager, name: str):
    """Generate a standard save file.

    .. Keyword Arguments:
    :param data:
    :param name: Path to the new file. ".json" is automatically added.
    Path is automatically preceded by "data/saves/".

    .. Types:
    :type data: DataManager
    :type name: str

    .. Returns:
    :return: 0 when successful.
    :rtype: int

    """
    if name.split(".")[-1] != "json":
        data.save_all("data/saves/" + name + ".json")
    else:
        data.save_all("data/saves/" + name)

    return 0


def make_single_image(data: DataManager, image_path: str):
    """Simulate a single image.

    .. Keyword Arguments:
    :param data:
    :param image_path: Path to image.

    .. Types:
    :type data:
    :type image_path: str

    .. Returns:
    :return:
    :rtype:

    """
    default_vals = HRTEM.get_default_parameter_dict()
    for key, value in default_vals.items():
        if data.hrtem[key]["Value"] != value:
            log(f"{key}: {data.hrtem[key]['Value']}", "parameters")

    if image_path[-1] == "/":
        name = data.save_file_path.split("/")[-1].split(".")[0]
    else:
        name = ""
    base = "data/simulated/"

    if image_path.split(".")[-1] != "zarr":
        final_image_path = f"{base}{image_path}{name}.zarr"
    else:
        final_image_path = f"{base}{image_path}{name}"

    # if image_path.split(".")[-1] != "tiff":
    #     final_image_path = f"{base}{image_path}{name}.tiff"
    # else:
    #     final_image_path = f"{base}{image_path}{name}"

    # # abtem.show_atoms(picmaker_object.hrtem.crystal_model)
    # # wave.show(cmap="gray")
    # # plt.show()

    picmaker_object = PicMaker(data.copy())
    picmaker_object.simulate_image()

    # shifted = measurement - np.min(measurement, keepdims=True)
    # normalized = shifted / np.sum(shifted, keepdims=True)

    # import abtem

    # sampling = data.lateral_sampling
    # image_measurement = abtem.measurements.Images(normalized, (sampling, sampling))
    # image_measurement.to_tiff(final_image_path)

    picmaker_object.hrtem.image_wave.to_zarr(final_image_path, overwrite=True)

    return final_image_path


def randomize_parameters(data):
    """
    Randomize parameters in their given limits.

    .. Keyword Arguments:
    :param data: all parameters

    .. Types:
    :type data: DataManager

    .. Returns:
    :return: DataManager-object
    :rtype: DataManager

    """
    randomized_data = data.copy()

    for parameter in randomized_data.parameters:
        if not parameter["Fixed"]:
            min_ = parameter["Min"]
            max_ = parameter["Max"]
            rand = np.random.rand()
            parameter["Value"] = rand * (max_ - min_) + min_

    return randomized_data


def manipulate_parameters(data, edit_parameter):
    """Change parameters on the fly.

    .. Keyword Arguments:
    :param data: all parameters
    :param edit_parameter: [[key1, ..., last_key, value], [...]]

    .. Types:
    :type data: DataManager
    :type edit_parameter: list of lists of strings

    .. Returns:
    :return: data

    """
    if data.save_file_path == "":
        data.save_file_path = "/tmp/hrtem/dummy.json"

    path = data.save_file_path
    data.save_all(path)

    jdict = {}
    with open(path, "r") as f:
        jdict = json.load(f)

    for parameter_route in edit_parameter:
        key_list = parameter_route[:-2]
        last_key = parameter_route[-2]
        new_value = parameter_route[-1]

        unpacked_dict = jdict
        for i in key_list:
            unpacked_dict = unpacked_dict[i]

        unpacked_dict[last_key] = float(new_value)

    with open(path, mode="w") as f:
        json.dump(jdict, f, indent=2)
    data.load_save_file(path)

    return data


def working_check(self):
    """Check if abtem is generally working."""
    import abtem
    import ase
    import numpy as np

    positions = [
        [0.0, 0.0, 0.0],
        [0.5, 0.5, 0.5],
        [0.0, 0.5, 0.5],
        [0.5, 0.0, 0.5],
        [0.5, 0.5, 0.0],
    ]

    compound = "SrTiO3"
    lattice_constant = 3.905
    cell = [lattice_constant, lattice_constant, lattice_constant]
    atoms_cell = ase.Atoms(compound, cell=cell, scaled_positions=positions)
    atoms_cell.center(vacuum=lattice_constant, axis=2)

    # atoms_cell = ase.build.bulk("Si", cubic=True)
    # atoms_cell = ase.io.read("./data/crystals/srtio3_100.cif")
    abtem.show_atoms(atoms_cell)

    atoms = atoms_cell * (1, 1, 10)

    potential = abtem.Potential(
        atoms, sampling=0.05, slice_thickness=0.5, parametrization="lobato"
    )
    potential = potential.build()

    wave = abtem.PlaneWave(energy=300e3)

    defocus = np.linspace(-80, 80, 5)

    measurements = (
        wave.multislice(potential)
        .apply_ctf(defocus=defocus, Cs=10e6, semiangle_cutoff=20, focal_spread=0)
        .intensity()
        .tile((1, 1))
        .compute()
        .poisson_noise(dose_per_area=2e3, seed=100)
    )

    measurements.show(
        explode=True,
        common_color_scale=True,
        figsize=(14, 5),
        cbar=True,
        cmap="gray",
    )

    plt.show()
