#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 18:42:27 2021.

@author: tristan
"""

from hrtem import HRTEM


class PicMaker:
    """Simulate image with fixed parameters."""

    def __init__(self, data):
        """Give fixed parameters via 'data'."""
        self.data = data

        self.hrtem = HRTEM(self.data)

    def simulate_image(self):
        """Just make a first simulation."""
        self.hrtem.first_sim()

        return self.hrtem.measurement
        # intensity = self.hrtem.measurement
        # x, y = self.hrtem.measurement.shape
        # mu = self.data.electrons
        # sig = self.data.variance
        # if self.data.poisson:
        #     avrg_intensity = np.sum(intensity) / x / y
        #     intensity = np.random.poisson(intensity / avrg_intensity * mu)
        # if self.data.gaussian:
        #     gauss = np.random.normal(mu, sig, x*y)
        #     gauss = gauss.reshape((x, y))
        #     avrg_intensity = np.sum(intensity) / x / y
        #     intensity = intensity * (mu / avrg_intensity)
        #     intensity += gauss

        # rotated_intensity = np.rot90(intensity)

        # return rotated_intensity
