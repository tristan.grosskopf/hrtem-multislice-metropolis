#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 08:47:41 2021.

@author: tristan
"""

import tui
import dmanager as dm
import sys
from os import path, mkdir
from log import log, head_log


def ensure_that_necessary_directories_exist():
    """
    Create the following directories if they don't exist.

    - data
    - data/crystals: cif-files, but kinda obsolete since definition of crystal
    is integrated in save-file
    - data/saves: save-files contain all parameters to do a simulation
    - data/results: folders named by time stamps that contain the csv with all
    the parameters and how they developed during the simulation
    - data/evaluation: mostly graphs
    - tmp/hrtem: put last simulated image here
    """
    if not path.isdir("data"):
        mkdir("data")

    if not path.isdir("data/crystals"):
        mkdir("data/crystals")

    if not path.isdir("data/saves"):
        mkdir("data/saves")

    if not path.isdir("data/results"):
        mkdir("data/results")

    if not path.isdir("data/evaluation"):
        mkdir("data/evaluation")

    if not path.isdir("/tmp/hrtem"):
        mkdir("/tmp/hrtem")


def main():
    """Enter program."""
    head_log()

    ensure_that_necessary_directories_exist()

    # Build DataManager at "top" so it can be manipulated and potentially serve
    # for data exchange between classes.
    data = dm.DataManager()

    if len(sys.argv) == 1:
        from PyQt5.QtWidgets import QApplication
        import gui

        app = QApplication(sys.argv)
        gui.App(data)
        sys.exit(app.exec_())
    else:
        log(str(sys.argv[1:]), "main")
        tui.TextUserInterface(data)

    return 0


if __name__ == "__main__":
    main()
