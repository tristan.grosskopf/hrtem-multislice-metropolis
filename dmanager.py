#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 16:03:25 2021

@author: tristan
"""

from os import listdir, path as ospath
from os.path import isfile
from os.path import isdir
from hrtem import HRTEM
from importlib import import_module
from scipy.ndimage.interpolation import rotate
from copy import deepcopy
from abtem import from_zarr
import constants
from log import log
import json
import hyperspy.api as hs
import numpy as np
import matplotlib.pyplot as plt


class DataManager:
    elements = constants.elements
    properties = constants.properties

    def __init__(self):
        # image parameters
        self.rotation_angle = None
        self.estimated_positions = {}

        self.recording_intensity = None
        self.rotated_intensity = None

        # simulation control parameters
        self.iterations = 100
        self.runs = 1
        self.schedule = False
        self.schedule_path = ""
        self.slice_thickness = 0.5
        self.lateral_sampling = 0.05
        self.annealing_constant = 2
        self.defocus_series = False
        self.defocus_images = 1

        # model parameters
        self.model = constants.sto

        # simulated HRTEM parameters
        self.selected_model = {}
        default_parameters = HRTEM.get_default_parameter_dict()
        self.hrtem = self.generate_empty_parameters(default_parameters)
        self.usr_parameter_names = []

        self.run_no = 0

        self.poisson = False
        self.gaussian = False
        self.variance = 0.0
        self.electrons = 0

        self.save_file_path = ""
        self.recording_path = ""

        self.model_function = None

        self.simulated_intensity = None

    def save_all(self, path: str):
        self.save_file_path = path

        with open(path, mode="w") as f:
            jdict = {}

            jdict["image"] = {}
            jdict["image"]["recording"] = self.recording_path
            jdict["image"]["angle"] = self.rotation_angle
            jdict["image"]["positions"] = self.estimated_positions

            jdict["model"] = self.model

            jdict["sim_control"] = {}
            jdict["sim_control"]["iterations"] = self.iterations
            jdict["sim_control"]["runs"] = self.runs
            jdict["sim_control"]["defocus_series"] = self.defocus_series
            jdict["sim_control"]["schedule"] = self.schedule
            jdict["sim_control"]["sched_path"] = self.schedule_path
            jdict["sim_control"]["slice"] = self.slice_thickness
            jdict["sim_control"]["sampling"] = self.lateral_sampling
            jdict["sim_control"]["annealing"] = self.annealing_constant
            jdict["sim_control"]["poisson"] = self.poisson
            jdict["sim_control"]["gaussian"] = self.gaussian
            jdict["sim_control"]["variance"] = self.variance
            jdict["sim_control"]["electrons"] = self.electrons

            jdict["hrtem"] = self.hrtem

            json.dump(jdict, f, indent=2)

        log("Saved under: " + path, __name__)
        return 0

    def load_save_file(
        self, path, image=None, model=None, sim_control=None, hrtem=None
    ):
        if not isfile(path):
            log("Error: Save file not existing", __name__)
            return 0

        self.save_file_path = path

        jdict = {}
        with open(path, "r") as f:
            jdict = json.load(f)

        if image is None:
            self.load_image(jdict["image"])
        else:
            image_dict = {}
            with open(image, "r") as f:
                image_dict = json.load(f)["image"]
            self.load_image(image_dict)

        if model is None:
            self.load_model(jdict["model"])
        else:
            model_dict = {}
            with open(model, "r") as f:
                model_dict = json.load(f)["model"]
            self.load_model(model_dict)

        if sim_control is None:
            self.load_sim_control(jdict["sim_control"])
        else:
            sim_control_dict = {}
            with open(sim_control, "r") as f:
                sim_control_dict = json.load(f)["sim_control"]
            self.load_sim_control(sim_control_dict)

        if hrtem is None:
            self.load_hrtem(jdict["hrtem"])
        else:
            hrtem_dict = {}
            with open(hrtem, "r") as f:
                hrtem_dict = json.load(f)["hrtem"]
            self.load_hrtem(hrtem_dict)

    def load_image(self, image_dict):
        self.estimated_positions = image_dict["positions"]
        self.recording_path = image_dict["recording"]
        self.rotation_angle = image_dict["angle"]

        self.read_recording(self.recording_path)

    def load_model(self, model_dict):
        self.model = model_dict

        self.update_module_func()

    def load_sim_control(self, sim_control_dict):
        self.iterations = sim_control_dict["iterations"]
        self.runs = sim_control_dict["runs"]
        self.defocus_series = sim_control_dict["defocus_series"]
        self.schedule = sim_control_dict["schedule"]
        self.schedule_path = sim_control_dict["sched_path"]
        self.slice_thickness = sim_control_dict["slice"]
        self.lateral_sampling = sim_control_dict["sampling"]
        self.annealing_constant = sim_control_dict["annealing"]
        self.poisson = sim_control_dict["poisson"]
        self.gaussian = sim_control_dict["gaussian"]
        self.variance = sim_control_dict["variance"]
        self.electrons = sim_control_dict["electrons"]

    def load_hrtem(self, hrtem_dict):
        self.hrtem = hrtem_dict

        self.pad_parameters()

    def pad_parameters(self):
        params = self.hrtem
        for p in self.hrtem:
            if params[p]["Radius"] == 0:
                params[p]["Radius"] = (params[p]["Max"] - params[p]["Min"]) / 2

    def create_crystal_model(self, name, chemical_formula):
        self.crystal_models[name] = {}
        model = self.crystal_models[name]
        model["Name"] = name
        model["Formula"] = chemical_formula
        model["Custom"] = False

        atoms = self.parse_formula(chemical_formula)
        # TODO: Extend with lattice constants b, c and angles ab, ac, bc
        wyckoff = {atom: [0.0, 0.0, 0.0] for atom in atoms}
        model["Atoms"] = wyckoff
        model["Lattice Constant"] = 0.0
        model["Dimensions"] = {"x": 1, "y": 1, "z": 1}

        model["Code"] = ""

        self.selected_model = self.crystal_models[name]
        self.save_model()

    def save_model(self):
        model = self.selected_model
        with open("data/crystals/" + model["Name"], "w") as f:
            json.dump(model, f, indent=2)

        self.update_module_func()

    def make_picture(self):
        recording = self.rotated_intensity

        plot_ar = recording - np.min(recording)
        plot_ar = plot_ar / np.max(plot_ar)
        plt.imsave("/tmp/hrtem/recording.png", plot_ar)

    def read_recording(self, path):
        """Load and normalize recordings.

        The goal is to fill recording_intensities with something. Note that
        this is a list in order to handle defocus series.
        """
        if path is None or path == "":
            return 0

        if len(path.split("/")) == 1:
            path = f"data/recordings/{path}"

        if "zarr" in path:
            intensity = np.abs(from_zarr(path).compute().array)
            # intensity = intensity - np.min(intensity)
            # intensity = intensity / np.sum(intensity)
            self.recording_intensities = [intensity]
            return 0

        if not isdir(path) and not isfile(path):
            # Nothing exists under the given path
            log(f"Error: Couldn't load recording under {path}", __name__)
            self.recording_path = ""

            # load dummy
            self.recording_intensities = [np.ones((10, 10))]
        elif isfile(path):
            self.recording_intensities = [self.read_single_recording(path)]
        else:
            # This is for loading defocus series.
            self.recording_intensities = []
            for r in listdir(path):
                if r.split(".")[-1] == "hdf5":
                    file_path = f"{path}/{r}"
                    intensity = self.read_single_recording(file_path)
                    self.recording_intensities.append(intensity)

            self.defocus_images = len(self.recording_intensities)

        self.make_picture()

    def read_single_recording(self, path):
        """Load and normalize recording."""
        intensity = hs.load(path).data
        intensity = intensity - np.min(intensity)
        intensity = intensity / np.sum(intensity)

        # first case means that a save file has been loaded
        if self.recording_path == ospath.relpath(path):
            self.rotate_recording(intensity)
        # this case means that just a new recording is loaded
        else:
            self.recording_path = ospath.relpath(path)
            self.rotated_intensity = intensity

        return intensity

    def rotate_recording(self, intensity):
        angle = self.rotation_angle
        if angle is not None:
            self.rotated_intensity = rotate(intensity, -angle)
        else:
            self.rotated_intensity = intensity

    def parse_formula(self, name):
        symbols = []

        if len(name) == 0:
            return []
        elif name[0].islower():
            return []
        elif name[0].isdigit():
            return []

        for k, c in enumerate(name):
            if c.isupper():
                symbols.append(c)
            elif c.islower():
                symbols[-1] += c
            elif c.isdigit():
                if name[k - 1].isdigit():
                    continue

                subscript = int(c)
                for d in name[(k + 1) :]:
                    if d.isdigit():
                        subscript *= 10
                        subscript += int(d)

                toappend = symbols.pop(-1)
                if not (toappend in self.elements):
                    return []
                for i in range(1, subscript + 1):
                    symbols.append(toappend + str(i))

        return symbols

    def parse_module(self):
        path = self.selected_model["Code"]

        if not isfile(path):
            print("Error: Couldn't find module in '" + path + "'.")
            return 0

        head = ""

        with open(path, "r") as module:
            for line in module:
                if "def" in line.split(" "):
                    head = line
                    break

        begin = list(head).index("(") + 1
        end = list(head).index(")")

        head = head[begin:end].replace(" ", "")
        parameters = head.split(",")[4:]

        if self.usr_parameter_names == parameters:
            return 0

        for p in self.usr_parameter_names:
            self.hrtem.pop(p)

        self.usr_parameter_names = parameters

        for p in parameters:
            pdict = {prop: 0.0 for prop in self.properties}
            self.hrtem[p] = pdict

    def set_estimated_position(self, atom, xy):
        self.estimated_positions[atom] = xy

    def get_estimated_position(self, atom):
        if atom in self.estimated_positions:
            return self.estimated_positions[atom]
        else:
            return [0, 0]

    def generate_empty_parameters(self, default_parameters):
        param_dict = {}

        for p in default_parameters:
            param_dict[p] = {o: 0.0 for o in self.properties}
            param_dict[p]["Value"] = default_parameters[p]
            param_dict[p]["Fixed"] = 1

        return param_dict

    def get_parameters_to_find(self):
        """Get list of names of parameters that are not fixed."""
        parameters_to_find = []

        for k in self.hrtem:
            if not self.hrtem[k]["Fixed"]:
                parameters_to_find.append(k)
                if k == "c10":
                    for _ in range(self.defocus_images - 1):
                        parameters_to_find.append("c10")

        # Paradigm: It's not necessary and shouldn't be done that the result is
        # stored in a global variable. This would only lead to complications
        # because one has to think about when to update it. With this you just
        # have to make sure that it isn't calculated again and again in the
        # main loop of the simulation which is arguably easier to ensure.
        return parameters_to_find

    def get_discrete_parameters(self, parameters_to_find):
        """Get discrete parameters.

        Get positions (indices) of parameters in 'parameters_to_find' that
        are discrete (i.e. have not a precision of 0.0).

        .. Keyword Arguments:
        :param parameters_to_find:

        .. Types:
        :type parameters_to_find:

        .. Returns:
        :return:
        :rtype:
        """
        discrete_parameters = []

        for i, k in enumerate(parameters_to_find):
            if self.hrtem[k]["Precision"] != 0:
                discrete_parameters.append(i)

        return discrete_parameters

    def get_property_dictionaries(self, parameters):
        """Get dictionary of properties.

        Create dictionary of the form {Property: Value} of all the
        parameters.
        """
        property_dicts = []

        for p in parameters:
            property_dicts.append(self.hrtem[p].copy())

        return property_dicts

    def set_parameter(self, prop, parameter=None, array=None, value=None):
        parameters_to_find = self.get_parameters_to_find()

        if array is not None:
            if isinstance(array, list) or isinstance(array, np.ndarray):
                for k, param in enumerate(parameters_to_find):
                    self.hrtem[param][prop] = array[k]
            else:
                print("Couldn't deal with type " + type(array) + " of array.")
                return -1

        elif parameter is not None and value is not None:
            para_dict = None
            if parameter in self.hrtem:
                para_dict = self.hrtem
            else:
                print("The parameter " + parameter + " is not known.")
                return -1

            # single value gets delivered
            para_dict[parameter][prop] = value

            # adjustments depending on changed property
            if prop == "Min" or prop == "Max":
                mi = para_dict[parameter]["Min"]
                ma = para_dict[parameter]["Max"]
                if mi > ma:
                    para_dict[parameter]["Min"] = ma
                    para_dict[parameter]["Max"] = mi
                    mi = para_dict[parameter]["Min"]
                    ma = para_dict[parameter]["Max"]
                para_dict[parameter]["Radius"] = (ma - mi) / 2

        else:
            print("Too few arguments")
            return -1

    def get_parameter(self, prop, parameter=None):
        """Return list of the same property of all parameters."""
        parameters_to_find = self.get_parameters_to_find()

        if parameter is None:
            prop_list = []

            for param in parameters_to_find:
                prop_list.append(self.hrtem[param][prop])

            return prop_list

        else:
            return self.hrtem[parameter][prop]

    def get_parameter_arrays(self):
        """Return tuple of the following arrays.

        vals: Values of all parameters
        radii: Radii of all parameters
        mins: Min values of all parameters
        maxs: Max values of all parameters

        uses data.get_parameter(prop, parameter=None)
        """
        vals = np.array(self.get_parameter("Value"))
        radii = np.array(self.get_parameter("Radius"))
        mins = np.array(self.get_parameter("Min"))
        maxs = np.array(self.get_parameter("Max"))

        return (vals, radii, mins, maxs)

    def round_down_to_discrete_value(self, parameter_props, continuous_value):
        precision = parameter_props["Precision"]

        return (continuous_value // precision) * precision

    def update_module_func(self):
        model = self.model

        if model["Code"] != "" and model["Custom"] is True:
            module = import_module(model["Code"][:-3])
            self.model_function = list(module.__dict__.values())[-1]
            self.parse_module()
        else:
            self.model_function = None

    def wrapper(self, func, args):
        return func(*args)

    def get_custom_3d_model(self):
        model = self.selected_model
        formula = model["Formula"]
        wyckoff = model["Atoms"]
        atoms = list(model["Atoms"].keys())
        vectors = [wyckoff[atom] for atom in atoms]
        lattice_constant = model["Lattice Constant"]
        dimensions = model["Dimensions"]

        params = self.usr_parameters[model["Code"]]

        usr_params = [params[p]["Value"] for p in params]

        args = [formula, vectors, dimensions, lattice_constant]
        args += usr_params

        return self.wrapper(self.model_function, args)

    def copy(self):
        return deepcopy(self)


if __name__ == "__main__":
    data = DataManager()
    data.load_settings("data/saves/sample_test.json")
    hrtem = HRTEM(data)
    print(hrtem.dependent_img_int_paras)
    print(hrtem.image_intensity_grid)
