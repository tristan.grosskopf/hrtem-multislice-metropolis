#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 06 13:07:00 2023.

@author: tristan
"""

import numpy as np
from ase import Atoms
from abtem import Potential, PlaneWave
from abtem.transfer import CTF
from ase.visualize import view
from inspect import signature
from log import log


class HRTEM:
    """
    Propagate electron wave.

    This class models the HRTEM as an information channel which alters
    the phase of the electron wave.
    """

    @staticmethod
    def get_default_parameter_dict():
        """Extract default parameter values."""
        t = [
            HRTEM.make_crystal,
            HRTEM.make_potential,
            HRTEM.make_exit_plane_wave,
            HRTEM.make_ctf,
            HRTEM.make_image_wave,
            HRTEM.make_mtf,
            HRTEM.make_measurement,
            HRTEM.make_shift,
        ]

        parameter_dict = {}
        for func in t:
            sig = signature(func).parameters.values()
            for s in sig:
                if s.name != "self":
                    parameter_dict[s.name] = s.default

        return parameter_dict

    def __init__(self, data):
        """Initialize lots of important objects."""
        self.data = data

        # current objects
        self.crystal_model = None
        self.crystal_potential = None
        self.exit_plane_wave = None
        self.ctf = None
        self.mtf = None
        self.image_wave = None
        self.measurement = None
        self.shift = None

        self.old_crystal_model = None
        self.old_crystal_potential = None
        self.old_exit_plane_wave = None
        self.old_ctf = None
        self.old_mtf = None
        self.old_image_wave = None
        self.old_image_intensity = None
        self.old_shift = None

        # list of list of... of index of object in object_list
        self.structure_grid = []
        self.potential_grid = []
        self.exit_pw_grid = []
        self.image_wave_grid = []
        self.image_intensity_grid = []
        self.ctf_grid = []
        self.mtf_grid = []
        self.shift_grid = []

        # already calculated objects
        self.structure_list = []
        self.potential_list = []
        self.exit_pw_list = []
        self.image_wave_list = []
        self.image_intensity_list = []
        self.ctf_list = []
        self.mtf_list = []
        self.shift_list = []

        # remember last kwargs
        self.structure_kwargs = []
        self.potential_kwargs = []
        self.exit_pw_kwargs = []
        self.image_wave_kwargs = []
        self.image_intensity_kwargs = []
        self.ctf_kwargs = []
        self.mtf_kwargs = []
        self.shift_kwargs = []

        # dictionaries will contain parameter name and default value
        self.structure_parameter_names = {}
        self.potential_parameter_names = {}
        self.exit_pw_parameter_names = {}
        self.ctf_parameter_names = {}
        self.image_wave_parameter_names = {}
        self.mtf_parameter_names = {}
        self.image_intsty_parameter_names = {}
        self.shift_parameter_names = {}

        # bools in the order of parameters_to_find that tell if the
        # corresponding parameter has been changed
        self.updated = []

        # [names of variable parameters]
        parameters_to_find = self.data.get_parameters_to_find()

        self.init_parameter_lists()
        self.init_all_result_grids(parameters_to_find)

        # make sure that CTF uses same energy as exit plane wave with
        # this exchange variable
        self.same_energy = 0

    def init_parameter_lists(self):
        """Construct lists containing all the parameters a stage depends on."""
        t = [
            (self.make_crystal, self.structure_parameter_names),
            (self.make_potential, self.potential_parameter_names),
            (self.make_exit_plane_wave, self.exit_pw_parameter_names),
            (self.make_ctf, self.ctf_parameter_names),
            (self.make_image_wave, self.image_wave_parameter_names),
            (self.make_mtf, self.mtf_parameter_names),
            (self.make_measurement, self.image_intsty_parameter_names),
            (self.make_shift, self.shift_parameter_names),
        ]

        for func, names in t:
            names.clear
            sig = signature(func).parameters.values()
            for s in sig:
                if s.name != "self":
                    names[s.name] = s.default

        self.dependent_ctf_paras = list(self.ctf_parameter_names)
        self.dependent_mtf_paras = list(self.mtf_parameter_names)
        self.dependent_shift_paras = list(self.shift_parameter_names)

        self.dependent_strct_paras = list(self.structure_parameter_names)

        self.dependent_pot_paras = []
        self.dependent_pot_paras += self.dependent_strct_paras
        self.dependent_pot_paras += list(self.potential_parameter_names)

        self.dependent_exit_paras = []
        self.dependent_exit_paras += self.dependent_pot_paras
        self.dependent_exit_paras += list(self.exit_pw_parameter_names)

        self.dependent_img_wv_paras = []
        self.dependent_img_wv_paras += self.dependent_exit_paras
        self.dependent_img_wv_paras += self.dependent_ctf_paras
        self.dependent_img_wv_paras += list(self.image_wave_parameter_names)

        self.dependent_img_int_paras = []
        self.dependent_img_int_paras += self.dependent_img_wv_paras
        self.dependent_img_int_paras += self.dependent_mtf_paras
        self.dependent_img_int_paras += self.dependent_shift_paras
        image_int_para_name_list = list(self.image_intsty_parameter_names)
        self.dependent_img_int_paras += image_int_para_name_list

    def init_all_result_grids(self, parameters_to_find):
        """Make array of arrays for every stage."""
        iter = [
            (self.dependent_strct_paras, self.structure_grid),
            (self.dependent_pot_paras, self.potential_grid),
            (self.dependent_exit_paras, self.exit_pw_grid),
            (self.dependent_img_wv_paras, self.image_wave_grid),
            (self.dependent_img_int_paras, self.image_intensity_grid),
            (self.dependent_ctf_paras, self.ctf_grid),
            (self.dependent_mtf_paras, self.mtf_grid),
            (self.dependent_shift_paras, self.shift_grid),
        ]

        for parameter_names, grid in iter:
            grid.clear()
            grid += self.init_result_grid(parameters_to_find, parameter_names)

    def init_result_grid(self, parameters_to_find, parameters_of_stage):
        """Make array of arrays."""
        resulting_grid = []

        # discrete = [p for p in parameters_of_stage if ]

        # Firstly find out if all variable parameters are discrete
        all_discrete = True
        relevant_struct_params = []
        for p in parameters_of_stage:
            if p not in parameters_to_find:
                continue
            else:
                relevant_struct_params.append(p)
                if not self.data.hrtem[p]["Precision"]:
                    all_discrete = False
                    break

        # Secondly make a list of all parameters and generate the grid
        if all_discrete and len(relevant_struct_params) != 0:
            make = self.make_multi_dim_list_recursively
            resulting_grid = make(relevant_struct_params)

        return resulting_grid

    def make_multi_dim_list_recursively(
        self,
        parameters: dict[str, float],
    ) -> list:
        """Make the grid for a stage.

        .. Keyword Arguments:
        :param parameters: ["parameter1", "parameter2", ...]

        .. Types:
        :type parameters_to_find:

        .. Returns:
        :return: List of List of...
        :rtype:
        """
        data_parameters = self.data.hrtem
        precision = data_parameters[parameters[0]]["Precision"]
        min_ = data_parameters[parameters[0]]["Min"]
        max_ = data_parameters[parameters[0]]["Max"]

        extent = int((max_ - min_) // precision) + 1

        if len(parameters) == 1:
            return [None for _ in range(extent)]
        else:
            make = self.make_multi_dim_list_recursively
            return [make(parameters[1:]) for _ in range(extent)]

    def first_sim(self):
        """Make first simulation."""
        self.update_image(first_sim=True)

    def calculate_stage(
        self,
        stage_func,
        old_stage_object,
        old_stage_kwargs,
        argument_array,
        dependent_argument_array,
        stage_grid,
        object_list,
        follow,
    ):
        """Look into parameters of stage and update if necessary.

        .. Keyword Arguments:
        :param  self:
        :param stage_func:
        :param old_stage_object:
        :param old_stage_kwargs:
        :param argument_array: List of names of parameters of stage.
        :param dependent_argument_array:
        :param stage_grid:
        :param object_list:
        :param follow:

        .. Types:
        :type  self:
        :type stage_func:
        :type old_stage_object:
        :type argument_array:
        :type dependent_argument_array:
        :type stage_grid:
        :type object_list:
        :type follow:

        .. Returns:
        :return:
        :rtype:

        """
        kwargs = [self.data.hrtem[p]["Value"] for p in argument_array]

        # Follow might be 'False' but we have to check if relevant parameters
        # changed such that we have to recalculate the object.
        # When the old kwargs are None it means, that a first simulation needs
        # to be done.
        needs_recalculation = False
        if not follow:
            for arg, old_arg in zip(kwargs, old_stage_kwargs):
                if arg != old_arg:
                    needs_recalculation = True
                    break
        old_stage_kwargs.clear()
        for k in kwargs:
            old_stage_kwargs.append(k)

        # nothing has to be changed
        if not (needs_recalculation or follow):
            new_object = old_stage_object
        # no chance of finding a previously calculated object
        # this also means there are no discrete stages
        elif stage_grid == []:
            follow = True
            new_object = stage_func(*kwargs)
        else:
            follow = True
            grid_part = stage_grid
            index = -1
            for p in dependent_argument_array:
                precision = self.data.hrtem[p]["Precision"]
                # only consider discrete paremeters
                if precision == 0.0:
                    continue
                value = self.data.hrtem[p]["Value"]
                min = self.data.hrtem[p]["Min"]
                if index != -1:
                    grid_part = grid_part[index]
                index = int((value - min) // precision)
            if grid_part[index] is None:
                object_list.append(stage_func(*kwargs))
                grid_part[index] = len(object_list) - 1
                new_object = object_list[grid_part[index]]
            else:
                new_object = object_list[grid_part[index]]

        return (follow, new_object)

    def update_image(self, first_sim=False):
        """Go through all stages and update if needed.

        .. Returns:
        :return: No return yet.
        :rtype:

        """
        self.updated = []
        follow = first_sim

        follow, new_crystal = self.calculate_stage(
            self.make_crystal,
            self.crystal_model,
            self.structure_kwargs,
            self.structure_parameter_names,
            self.dependent_strct_paras,
            self.structure_grid,
            self.structure_list,
            follow,
        )
        self.old_crystal_model = self.crystal_model
        self.crystal_model = new_crystal
        self.updated.append(follow)

        follow, new_potential = self.calculate_stage(
            self.make_potential,
            self.crystal_potential,
            self.potential_kwargs,
            self.potential_parameter_names,
            self.dependent_pot_paras,
            self.potential_grid,
            self.potential_list,
            follow,
        )
        self.old_crystal_potential = self.crystal_potential
        self.crystal_potential = new_potential
        self.updated.append(follow)

        follow, new_exit_plane_wave = self.calculate_stage(
            self.make_exit_plane_wave,
            self.exit_plane_wave,
            self.exit_pw_kwargs,
            self.exit_pw_parameter_names,
            self.dependent_exit_paras,
            self.exit_pw_grid,
            self.exit_pw_list,
            follow,
        )
        self.old_exit_plane_wave = self.exit_plane_wave
        self.exit_plane_wave = new_exit_plane_wave
        self.updated.append(follow)

        # Attention: For this 'follow' is always 'False'
        branch_follow, new_ctf = self.calculate_stage(
            self.make_ctf,
            self.ctf,
            self.ctf_kwargs,
            self.ctf_parameter_names,
            self.dependent_ctf_paras,
            self.ctf_grid,
            self.ctf_list,
            first_sim,
        )
        self.old_ctf = self.ctf
        self.ctf = new_ctf
        self.updated.append(follow)

        follow, new_image_wave = self.calculate_stage(
            self.make_image_wave,
            self.image_wave,
            self.image_wave_kwargs,
            self.image_wave_parameter_names,
            self.dependent_img_wv_paras,
            self.image_wave_grid,
            self.image_wave_list,
            follow or branch_follow,
        )
        self.old_image_wave = self.image_wave
        self.image_wave = new_image_wave
        self.updated.append(follow)

        # Attention: For this 'follow' is always 'False'
        branch_follow, new_mtf = self.calculate_stage(
            self.make_mtf,
            self.mtf,
            self.mtf_kwargs,
            self.mtf_parameter_names,
            self.dependent_mtf_paras,
            self.mtf_grid,
            self.mtf_list,
            first_sim,
        )
        self.old_mtf = self.mtf
        self.mtf = new_mtf
        self.updated.append(follow)

        # Attention: For this 'follow' is always 'False'
        _, new_shift = self.calculate_stage(
            self.make_shift,
            self.shift,
            self.shift_kwargs,
            self.shift_parameter_names,
            self.dependent_shift_paras,
            self.shift_grid,
            self.shift_list,
            first_sim,
        )
        self.old_shift = self.shift
        self.shift = new_shift
        self.updated.append(follow)

        follow, new_image_int = self.calculate_stage(
            self.make_measurement,
            self.measurement,
            self.image_intensity_kwargs,
            self.image_intsty_parameter_names,
            self.dependent_img_int_paras,
            self.image_intensity_grid,
            self.image_intensity_list,
            follow or branch_follow,
        )
        self.old_image_intensity = self.measurement
        self.measurement = new_image_int
        self.updated.append(follow)

    def revert_changes(self):
        """Reset all changes by loading old intermediate stages."""
        if self.updated[0]:
            self.crystal_model = self.old_crystal_model
        if self.updated[1]:
            self.crystal_potential = self.old_crystal_potential
        if self.updated[2]:
            self.exit_plane_wave = self.old_exit_plane_wave
        if self.updated[3]:
            self.ctf = self.old_ctf
        if self.updated[4]:
            self.image_wave = self.old_image_wave
        if self.updated[5]:
            self.mtf = self.old_mtf
        if self.updated[6]:
            self.shift = self.old_shift
        if self.updated[7]:
            self.measurement = self.old_image_intensity

    # --------------------------------------------------------------------------

    def make_std_crystal(self, dimensions, lattice_constant):
        x = dimensions["x"]
        y = dimensions["y"]
        z = dimensions["z"]
        # TODO: Adjust for arbitrary unit cells

        model = self.data.model
        compound = model["Formula"]
        cell = [lattice_constant, lattice_constant, lattice_constant]

        wyckoff = model["Atoms"]
        atoms = list(model["Atoms"].keys())
        positions = [wyckoff[atom] for atom in atoms]

        crystal = Atoms(compound, cell=cell, scaled_positions=positions)
        crystal = crystal.repeat((x, y, z))
        # crystal *= (x, y, z)
        # crystal.set_pbc([1, 1, 0])
        # crystal.center(vacuum=lattice_constant, axis=2)

        return crystal

    def make_crystal(self, lattice_constant=1.0, z=1):
        if self.data.model["Code"] != "":
            custom_func = self.data.get_custom_3d_model
        else:
            custom_func = None

        if custom_func is None:
            x = int(self.data.model["x"])
            y = int(self.data.model["y"])
            # z = int(self.data.model["z"])
            z = int(z)
            dimensions = {"x": x, "y": y, "z": z}
            model = self.make_std_crystal(dimensions, lattice_constant)
        else:
            model = custom_func()

        return model

    def show_crystal(self):
        self.make_crystal()

        view(self.crystal_model)

    def make_potential(self):
        sampling = self.data.lateral_sampling
        slice_thickness = self.data.slice_thickness
        potential = Potential(
            self.crystal_model,
            sampling=sampling,
            slice_thickness=slice_thickness,
            parametrization="lobato",
        )
        return potential.build()

    def make_exit_plane_wave(self, energy=3e5):
        wave = PlaneWave(energy=energy)
        # wave.grid.match(self.crystal_potential)
        # wave = wave.build()

        # propagator = FresnelPropagator()

        # for potential_slice in self.crystal_potential:
        #     potential_slice.transmit(wave)
        #     propagator.propagate(wave, potential_slice.thickness)

        self.same_energy = energy
        return wave.multislice(self.crystal_potential)

    def make_ctf(
        self,
        focal_spread=30.0,
        angular_spread=0.0,
        semiangle_cutoff=30,
        c10=0.0,
        c12=0.0,
        phi12=0.0,
        c21=0.0,
        phi21=0.0,
        c23=0.0,
        phi23=0.0,
        c30=0.0,
    ):
        energy = self.same_energy
        aberration_coefficients = {
            "C10": c10,
            "C12": c12,
            "phi12": phi12,
            "C21": c21,
            "phi21": phi21,
            "C23": c23,
            "phi23": phi23,
            "C30": c30,
        }
        ctf = CTF(
            aberration_coefficients=aberration_coefficients,
            energy=energy,
            focal_spread=focal_spread,
            angular_spread=angular_spread,
            semiangle_cutoff=30,
        )
        return ctf

    def make_image_wave(self):
        return self.exit_plane_wave.apply_ctf(self.ctf)
        # sub = self.exit_plane_wave.multislice(self.crystal_potential)
        # return sub.apply_ctf(self.ctf)

    def make_mtf(
        self,
        binning=1.0,
        const=-0.040557814,
        l1_a=2.51763510e-06,
        l1_b=3.25017295e-05,
        l2_a=4.41962918e-02,
        l2_b=9.37672392e-02,
        g_a=4.42445688e-01,
        g_b=4.30419776e02,
    ):
        preserve_intensity = True
        dx_image_over_dx_px = None
        nx, ny = self.exit_plane_wave.gpts
        ky, kx = np.mgrid[-ny / 2 : ny / 2, -nx / 2 : nx / 2]
        q_squared = kx**2 / nx**2 + ky**2 / ny**2

        # if no ratio of image scale dx_image over pixel size dx_px of the
        # modelled camera (i.e. the scale of the experimental image) is given
        # they are assumed to be euqal, the MTF is adjusted accordingly
        # otherwise
        if dx_image_over_dx_px is not None:
            # scales the argument of the parametrized MTF according to the
            # ratio of image scale dx_image over pixel size dx_px of the
            # modelled camera (i.e. the scale of the experimental image)
            q_squared /= dx_image_over_dx_px**2

        # potential binning of the modelled camera is taken into account in the
        # next lines since the MTF is typically parametrized for binning=1
        term1 = l1_a / (l1_b + q_squared / binning**2)
        term2 = l2_a / (l2_b + q_squared / binning**2)
        term3 = g_a * np.exp(-g_b * q_squared / binning**2)
        MTF = const + term1 + term2 + term3  # scintillator MTF

        MTF *= np.sinc(np.sqrt(q_squared))  # pixelation effect
        MTF = np.fft.fftshift(MTF)

        if preserve_intensity:
            # make sure the MTF doesn't change the mean intensity unless stated
            # differently
            MTF[0, 0] = 1

        # It returns a lambda function in order to get something that can be
        # applied on the intensity (similar to the ctf).
        # TODO Make it work for non quadratic intensity grids.
        return lambda img: np.real(np.fft.ifft2(np.fft.fft2(img) * MTF)).T

    def make_shift(self, x_translation=0.0, y_translation=0.0):
        return [x_translation, y_translation]

    def make_measurement(self):
        # image_wave_array = np.rot90(self.image_wave.array)
        # image_intensity = np.abs(image_wave_array) ** 2

        # return self.mtf(image_intensity)
        return np.abs(self.image_wave.array.compute())


if __name__ == "__main__":
    import dmanager as dm

    data = dm.DataManager()
    data.load_save_file("./data/saves/sample_discrete.json")

    data.count_defocus = 1
    hrtem_obj = HRTEM(data)

    parameters_to_find = data.get_parameters_to_find()
    hrtem_obj.init_all_result_grids(parameters_to_find)
    print(hrtem_obj.image_wave_grid)
    print(hrtem_obj.ctf_grid)
