#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created 16. August 2023 at 09:48.

@author: tristan
"""

import unittest
from unittest.mock import patch  # replace sys.argv
import main
import sys
from os import remove
from os.path import isfile


class TestHRTEM(unittest.TestCase):
    """Test the functionality of the whole program."""

    def test_generate(self):
        """Generate a sample save file."""
        if isfile("data/saves/sample.json"):
            remove("data/saves/sample.json")

        testargs = ["main", "--generate-save-file"]
        with patch.object(sys, "argv", testargs):
            main.main()
            assert isfile("data/saves/sample.json")

    def test_make_image(self):
        """Simulate using the sample save file from the previous method."""
        tiff = "data/recordings/sample/sample.tiff"

        if isfile(tiff):
            remove(tiff)

        edit_param = (
            "-e model x 3 -e model y 3 -e model z 3 -e sim_control sampling 0.01"
        )
        testargs = f"main --make-image sample.json {edit_param}".split()
        with patch.object(sys, "argv", testargs):
            main.main()
            assert isfile(tiff)


if __name__ == "__main__":
    unittest.main()
