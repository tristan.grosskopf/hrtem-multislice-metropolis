#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 15:01:19 2022.

@author: tristan
"""

import argparse
from annealing import SimulatedAnnealing
from evaluate import Evaluate
from os.path import isfile
from os.path import isdir
from os import listdir
from log import log
import test  # contains tests for unit testing
import unittest
import sys
import tui_helper


class TextUserInterface:
    """Interpret text given on command line.

    :__init__: Build the parser.
    """

    def __init__(self, data, args=None):
        """Build the parser."""
        self.data = data

        parser = argparse.ArgumentParser(
            prog="HRTEM Multislice Simulation",
            description="Fit HRTEM parameters to an experimental image.",
            epilog="",
        )

        parser.add_argument(
            "-t",
            "--test",
            action="store_true",
        )
        parser.add_argument(
            "-m",
            "--make-image",
            nargs="*",
            action="store",
            metavar=("SAVE_FILE(S)", "RESULT_PATH"),
            type=str,
        )
        parser.add_argument(
            "-g",
            "--generate-save-file",
            type=str,
            nargs="?",
            const="sample",
            default=None,
            metavar=("NAME"),
        )
        parser.add_argument(
            "-o",
            "--output-type",
            action="store",
            type=str,
        )
        parser.add_argument(
            "-f",
            "--fit-to-image",
            action="store",
            metavar=("SAVE_FILE(S)"),
            type=str,
        )
        parser.add_argument(
            "-e",
            "--edit-parameter",
            action="append",
            nargs="*",
        )
        parser.add_argument(
            "-c",
            "--compare",
            action="store",
            type=str,
        )
        parser.add_argument(
            "-p",
            "--plot-last-simulation",
            action="store_true",
        )
        parser.add_argument(
            "-w",
            "--working-check",
            action="store_true",
        )

        self.args = parser.parse_args(args)
        self.parse()

    def parse(self):
        """Switch for command line arguments."""
        test_ = self.args.test
        generate_save_file = self.args.generate_save_file
        fit_to_image = self.args.fit_to_image
        edit_parameter = self.args.edit_parameter
        make_image = self.args.make_image
        working_check = self.args.working_check

        # do unit testing
        if test_:
            sys.argv = [sys.argv[0]]
            unittest.main(module=test)
            return 0

        # generate a standard save file with default values given in hrtem.py
        elif generate_save_file is not None:
            save_file_name = generate_save_file
            tui_helper.generate_save_file(self.data, save_file_name)
            return 0

        # simulation based on given save file
        elif fit_to_image is not None:
            save_file = fit_to_image

            # check if save file exists
            if isfile("data/saves/" + save_file):
                self.data.load_save_file("data/saves/" + save_file)
            else:
                log(f"Error: Couldn't find '{save_file}'", __name__)
                raise Exception(f"Couldn't find {save_file}")
                return -1

            # last minute change of a parameter via command line
            if edit_parameter is not None:
                self.data = tui_helper.manipulate_parameters(edit_parameter)

            self.anneal_object = SimulatedAnnealing(self.data.copy())
            self.anneal_object.simulate()

            eval = Evaluate()
            eval.last_result()

        # simulate image with given parameters
        elif make_image is not None:
            # save_file contains all parameters
            # image_path is optional

            if len(make_image) == 1:
                # image_path default is data/recordings/
                save_file = make_image[0]
                image_path = f"{make_image[0].split('.')[0]}/"
            else:
                save_file, image_path = make_image

            iterate_or_not = []
            rel_path = f"data/saves/{save_file}"
            if isfile(rel_path):
                iterate_or_not = [rel_path]
            elif isdir(rel_path):
                iterate_or_not = [f"{rel_path}/{f}" for f in listdir(rel_path)]
            else:
                log(f"Error: Couldn't find '{save_file}'", __name__)
                raise Exception(f"Couldn't find {save_file}")
                return -1

            for sfile in iterate_or_not:
                self.data.load_save_file(sfile)

                helper = tui_helper

                if edit_parameter is not None:
                    self.data = helper.manipulate_parameters(self.data, edit_parameter)

                final_path = helper.make_single_image(self.data, image_path)

                log(f"[[file:{final_path}]]", __name__)

        elif self.args.plot_last_simulation:
            eval = Evaluate()
            eval.last_result()

        elif working_check:
            tui_helper.working_check()

        # ----------------------------------------------------------------------

        if self.args.compare is not None:
            eval = Evaluate()
            eval.compare("data/saves/" + self.args.compare)
