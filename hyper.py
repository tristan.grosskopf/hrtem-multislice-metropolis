#!/usr/bin/env python3
"""Teste das Laden von dm4-Dateien und ihre Visualisierung."""

import hyperspy.api as hs
from tifffile import imwrite

# import matplotlib.pyplot as plt
from shutil import rmtree
from os.path import isdir
from os import mkdir

s = hs.load(
    "data/experiment/230713_STO_focal_series/2_FocalSeries_fmin_-20nm_fmax_60nm_N_41.dm4"
)

if isdir("data/recordings/hyperspy/"):
    rmtree("data/recordings/hyperspy/")

# for i, c in enumerate(s):
#     c.change_dtype("uint8")
#     c.save(f"data/recordings/hyperspy/hyper_pic{i}.png")

# ss = [i for i in s]
# ar = ss[0].data[1000:1100, 1000:1100]
# print(ar)

mkdir("data/recordings/hyperspy/")
for i, c in enumerate(s):
    ar = c.data[1000:1100, 1000:1100]
    ar = ar / ar.max() * 255
    ar = ar.astype("uint8")
    imwrite(f"data/recordings/hyperspy/hyper_pic{i}.tif", ar)

# s.isig[0].plot()
# for i in s:
#     i.plot()
# s.plot(navigator="slider")
# plt.show()

# print(s)

# print(s.original_metadata)
