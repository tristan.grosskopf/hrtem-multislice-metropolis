#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26 11:02:54 2021

@author: tristan
"""

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng
from hrtem import HRTEM
from log import log
from os import mkdir
from os.path import isdir
from time import time
from time import gmtime
from PyQt5.QtCore import QObject, pyqtSignal
from tqdm import trange


class SimulatedAnnealing(QObject):
    # IO to GUI
    finished = pyqtSignal()
    progress = pyqtSignal(int)
    plotInfo = pyqtSignal(np.ndarray, str)

    def __init__(self, data, parent=None):
        """Simulated Annealing gets done here."""

        super(QObject, self).__init__(parent)

        self.data = data
        self.data.recording_intensity = self.data.rotated_intensity
        self.lattice_constant = data.hrtem["lattice_constant"]["Value"]

        self.old_offset = None
        self.offset = None
        self.base_offset = None
        self.old_shape = None
        self.shape = None
        self.cutout = None

        self.recording_patch = None

        self.results_path = ""
        self.results_file = None

        self.calculate_offset()

    def simulate(self):
        if isdir(self.data.recording_path):
            if not self.data.defocus_series:
                for r in self.data.recording_intensities:
                    self.data.recording_intensity = r
                    self.simulate_runs()
            else:
                self.simulate_runs()
        else:
            self.simulate_runs()

        self.finished.emit()

    def simulate_runs(self):
        for i in range(self.data.runs):
            self.data.run_no = i
            self.simulated_annealing()

    def calculate_offset(self):
        """
        Calculates number of pixels in x and y direction that the upper left
        corner of the fitted image is shifted relative to the upper left corner
        of the experimental picture.
        """

        model = self.data.selected_model
        positions = self.data.estimated_positions

        # Calculate lateral sampling
        est_pos = [a for a in positions if positions[a] != [0.0, 0.0]]
        vectors = {a: model["Atoms"][a][:2] for a in est_pos}
        if len(est_pos) > 1:
            sampling = 0
            pairs = 0

            for i, atom1 in enumerate(est_pos):
                for atom2 in est_pos[i + 1 :]:
                    pairs += 1
                    a1, b1 = positions[atom1]
                    a2, b2 = positions[atom2]
                    x1, y1 = vectors[atom1]
                    x2, y2 = vectors[atom2]

                    if x2 - x1 == 0 and y2 - y1 == 0:
                        pairs -= 1
                        continue

                    two_vals = True
                    x_sampling = 0
                    y_sampling = 0

                    if x2 - x1 != 0:
                        x_sampling = abs((x2 - x1) / (a2 - a1))
                    else:
                        two_vals = False

                    if y2 - y1 != 0:
                        y_sampling = abs((y2 - y1) / (b2 - b1))
                    else:
                        two_vals = False

                    if two_vals:
                        sampling += (x_sampling + y_sampling) / 2
                    else:
                        sampling += x_sampling + y_sampling

            sampling /= pairs
            sampling *= self.lattice_constant

            # change sampling only if calculated value deviates at least a
            # certain value from the entered sampling
            if abs(self.data.lateral_sampling - sampling) / sampling > 0.1:
                self.data.lateral_sampling = sampling

        # Calculate Offset
        sampling = self.data.lateral_sampling
        x_off = 0.0
        y_off = 0.0
        data_count = 0

        # y_dim needed to set selected unit cell as lower left corner
        y_dim = self.data.model["y"]

        # TODO: Adjust for arbitrary unit cells
        for atom in est_pos:
            data_count += 1
            vector = vectors[atom]
            x_pos = vector[0] * self.lattice_constant / sampling
            x_off += positions[atom][0] - x_pos
            y_pos = vector[1] * self.lattice_constant / sampling
            y_off += positions[atom][1] - y_pos

        if self.offset is not None:
            self.old_offset = self.offset.copy()

        if data_count != 0:
            x = int(x_off / data_count)
            y = self.data.recording_intensity.shape[0] - int(y_off / data_count)
            y = int(y - self.lattice_constant * y_dim / sampling)
            self.offset = [x, y]
            self.base_offset = [x, y]
        else:
            self.offset = [0, 0]
            self.base_offset = [0, 0]

    def residuum(self, simulated_intensity, n, shift):
        """Calculate difference between simulation and experiment.

        .. Keyword Arguments:
        :param simulated_intensity: numpy array
        :param n: index of defocus image
        :param shift:

        .. Returns:
        :return: squared difference of intensities per pixel
        :rtype: scalar

        """
        I_a = self.data.recording_intensities[n]
        I_b = simulated_intensity

        self.shape = I_b.shape
        (y_test, x_test) = I_b.shape

        lat_const = self.data.hrtem["lattice_constant"]["Value"]
        sampling = self.data.lateral_sampling
        self.old_offset = self.offset.copy()
        self.offset = [
            self.base_offset[0] + int(shift[0] * lat_const / sampling),
            self.base_offset[1] + int(shift[1] * lat_const / sampling),
        ]

        if self.old_offset != self.offset or self.old_shape != self.shape:
            cutx, cuty = [[0, 0], [0, 0]]
            patchx, patchy = [[0, 0], [0, 0]]

            x_off = self.offset[0]
            y_off = self.offset[1]

            (rec_y, rec_x) = I_a.shape

            if x_off > rec_x or x_off + x_test < 0:
                return -1
            if y_off > rec_y or y_off + y_test < 0:
                return -1

            if x_off < 0:
                if x_test + x_off > rec_x:
                    cutx = [-x_off, rec_x]
                    patchx = [0, rec_x]
                else:
                    cutx = [-x_off, x_test]
                    patchx = [0, x_test + x_off]
            else:
                if x_test + x_off > rec_x:
                    cutx = [0, rec_x - x_off]
                    patchx = [x_off, rec_x]
                else:
                    cutx = [0, x_test]
                    patchx = [x_off, x_test + x_off]

            if y_off < 0:
                if y_test + y_off > rec_y:
                    cuty = [-y_off, rec_y]
                    patchy = [0, rec_y]
                else:
                    cuty = [-y_off, y_test]
                    patchy = [0, y_test + y_off]
            else:
                if y_test + y_off > rec_y:
                    cuty = [0, rec_y - y_off]
                    patchy = [y_off, rec_y]
                else:
                    cuty = [0, y_test]
                    patchy = [y_off, y_test + y_off]

            self.old_shape = self.shape
            self.cutout = (cuty, cutx)
            self.patch = (patchy, patchx)

        cutx, cuty = self.cutout
        I_b = I_b[cutx[0] : cutx[1], cuty[0] : cuty[1]]
        I_b = I_b / np.sum(I_b)
        patchx, patchy = self.patch
        I_a = I_a[patchx[0] : patchx[1], patchy[0] : patchy[1]]
        I_a = I_a / np.sum(I_a)

        (Y, X) = I_b.shape
        return np.sum((I_a - I_b) ** 2) / X / Y

    def setup_save_file(self, parameters_to_find):
        now = gmtime()
        Y = str(now.tm_year)[2:]
        M = str(now.tm_mon)
        if len(M) == 1:
            M = "0" + M
        D = str(now.tm_mday)
        if len(D) == 1:
            D = "0" + D
        h = str(now.tm_hour + 1)
        if len(h) == 1:
            h = "0" + h
        m = str(now.tm_min)
        if len(m) == 1:
            m = "0" + m
        s = str(now.tm_sec)
        if len(s) == 1:
            s = "0" + s

        title = Y + M + D + "_" + h + m + s
        path = ""

        if self.data.runs > 1:
            if self.data.run_no == 0:
                self.save_path = "data/results/" + title + "_G/"
                path = self.save_path + title + "/"
                mkdir(self.save_path)
            else:
                path = self.save_path + title + "/"
        else:
            path = "data/results/" + title + "/"
            self.save_path = path

        self.results_path = path

        mkdir(path)

        if self.data.run_no == 0:
            if self.data.save_file_path != "":
                save_name = self.data.save_file_path.split("/")[-1]
                new_save_file = open(self.save_path + save_name, "w")
                with open(self.data.save_file_path, "r") as old_save_file:
                    for line in old_save_file:
                        new_save_file.write(line)
                new_save_file.close()

        final_path = f"{path}results_file.csv"
        results_file = open(final_path, "w")

        for parameter in parameters_to_find:
            results_file.write(parameter + ",")

        results_file.write("Residuum,Time\n")

        log(f"[[file:{path}]]", __name__)
        self.results_file = results_file

    def save(self, tup):
        arr = tup[0].copy()

        for e in tup[1:]:
            arr = np.append(arr, e)
        line = np.array2string(arr, separator=",")[1:-1].replace("\n", "")
        line = line.replace(" ", "")
        line += "\n"
        self.results_file.write(line)

    def update_covariance(self, i, cov, new_vals):
        mean0 = np.zeros(len(new_vals))
        mean1 = mean0
        mean0T = mean0[:, np.newaxis]
        mean1T = mean1[:, np.newaxis]
        new_valsT = new_vals[:, np.newaxis]
        sd = 5.76 / len(new_vals)

        cov_part = i * mean0 * mean0T - (i + 1) * mean1 * mean1T + new_vals * new_valsT
        cov = (i - 1) / i * cov + sd / i * cov_part

        return cov

    def simulated_annealing(self):
        start = time()

        # must be a list in case of a defocus series
        errors = [None for _ in range(self.data.defocus_images)]
        new_error = None

        hrtem = HRTEM(self.data)

        duration = self.data.iterations

        # list of names of parameters
        parameters_to_find = self.data.get_parameters_to_find()

        self.setup_save_file(parameters_to_find)

        gdp = self.data.get_discrete_parameters
        discrete_parameters = gdp(parameters_to_find)

        gpd = self.data.get_property_dictionaries
        property_dicts = gpd(parameters_to_find)

        check = duration // 10
        if check == 0:
            check = 1

        old_vals, radii, mins, maxs = self.data.get_parameter_arrays()

        hrtem.first_sim()
        error = self.residuum(hrtem.measurement, 0, hrtem.shift)

        parameter_count = len(parameters_to_find)
        cov = np.diag(np.ones(parameter_count))
        mean = np.zeros(parameter_count)
        new_vals = np.zeros(parameter_count)
        new_r = np.zeros(parameter_count)
        old_r = np.zeros(parameter_count)

        d_residuum = 0

        rng = default_rng()

        for i in trange(duration):
            self.progress.emit(i)
            self.save((np.abs(old_vals), np.abs(error), time() - start))

            if i > 0:
                cov = self.update_covariance(check, cov, old_r)

            while True:
                new_r = rng.multivariate_normal(mean, cov)
                new_vals = old_vals + new_r * radii

                enough = True
                for j in range(parameter_count):
                    if new_vals[j] < mins[j] or new_vals[j] > maxs[j]:
                        enough = False
                        break

                if enough:
                    break

            for j in discrete_parameters:
                v = self.data.round_down_to_discrete_value(
                    property_dicts[j], new_vals[j]
                )
                new_vals[j] = v

            for j, p in enumerate(parameters_to_find):
                self.data.hrtem[p]["Value"] = new_vals[j]

            hrtem.update_image()
            new_error = self.residuum(hrtem.measurement, 0, hrtem.shift)

            d_error = abs(new_error - error)
            d_residuum = 1 / (i + 1) * (i * d_residuum + d_error)

            if new_error <= error:
                old_r = new_r.copy()
                old_vals = new_vals.copy()
                error = new_error
            else:
                t = i / duration
                if t < 0.9:
                    beta = 10**3 * t
                else:
                    beta = 10**4 * t
                p = 10 ** (-d_error / d_residuum * beta)
                p_compare = np.random.rand(1)
                if p < p_compare:
                    hrtem.revert_changes()
                else:
                    error = new_error
                    old_r = new_r.copy()
                    old_vals = new_vals.copy()

        self.results_file.close()

        if self.data.run_no == 0:
            # offset must be refreshed one last time
            self.residuum(hrtem.measurement, 0, hrtem.shift)

            self.plot_result(hrtem.measurement)

    def plot_result(self, I_b):
        """Make final image.

        Make an image of the measured intensities with the simulation
        overlayed.

        .. Keyword Arguments:
        :param self:
        :param I_b: simulated picture

        .. Types:
        :type self:
        :type I_b: array of arrays

        """
        recording_intensity = self.data.recording_intensities[0]
        recording_intensity = recording_intensity - np.min(recording_intensity)
        recording_intensity = recording_intensity / np.sum(recording_intensity)

        cutx, cuty = self.cutout
        I_b = I_b[cutx[0] : cutx[1], cuty[0] : cuty[1]]
        I_b = I_b - np.min(I_b)
        I_b = I_b / np.sum(I_b)

        patchx, patchy = self.patch
        I_a = recording_intensity[patchx[0] : patchx[1], patchy[0] : patchy[1]]
        I_b = I_b * np.sum(I_a)

        recording_intensity[patchx[0] : patchx[1], patchy[0] : patchy[1]] = I_b

        (x_sample, y_sample) = recording_intensity.shape
        plot_ar = np.zeros((x_sample, y_sample, 3))
        for i in range(x_sample):
            for j in range(y_sample):
                v = recording_intensity[i, j]
                plot_ar[i, j] = (v, v, v)
        plot_ar = plot_ar - np.min(plot_ar)
        plot_ar = plot_ar / np.max(plot_ar)

        plt.imsave(self.save_path + "result.png", plot_ar)

        # recording_intensity = recording_intensity / np.sum(recording_intensity)
        # recording_intensity = np.rot90(recording_intensity)
        # self.plotInfo.emit(recording_intensity, self.save_path)
